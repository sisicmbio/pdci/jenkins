#!/bin/bash

set -ex

echo "${GIT_BRANCH}" >tmp.sh
sed -i "s/origin\///g" tmp.sh
rm -rf script_out
mkdir script_out
rm -rf log_sql
mkdir log_sql
NOME=$(cat tmp.sh)

PDCI_FILE_ENV_DOCKER=./.env.docker.hmg
echo ""
echo "Lendo arquivo ${PDCI_FILE_ENV_DOCKER}."
echo ""
#
#while IFS== read -r key value; do
#  printf -v "$key" %s "$value" && export "$key"

#done


## Set comma as delimiter
#IFS=','
#
##Read the split words into an array based on comma delimiter
#read -a strarr <<< "$text"

#export
#echo "PDCI_DB_NAME => $(sed 's/\x0D$//' ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//' | sed '/^$/d' | grep -v '^#' | grep 'PDCI_DB_NAME')";
echo ""
echo "  Carregando variavel PDCI_DB_NAME."
echo ""
export "$(sed 's/\x0D$//' ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//' | sed '/^$/d' | grep -v '^#' | grep 'PDCI_DB_NAME' | xargs)";
#echo "PDCI_DB_NAME =>${PDCI_DB_NAME}";


#echo "PDCI_DB_HOST => $(sed 's/\x0D$//' ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//' | sed '/^$/d' | grep -v '^#' | grep 'PDCI_DB_HOST')";
echo ""
echo "  Carregando variavel PDCI_DB_HOST."
echo ""
export "$(sed 's/\x0D$//' ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//' | sed '/^$/d' | grep -v '^#' | grep 'PDCI_DB_HOST' | xargs)";
#echo "PDCI_DB_NAME =>${PDCI_DB_HOST}";
#export
#exit 1
#
#while IFS= read -r line;
#do
#  echo "line => ${line}";
#
#  case $line in
#  PDCI_DB_NAME=*)
#    var1=${line%=*}
#    var2=${line#*=}
#    echo "var1 => ${var1}"
#    echo "var2 => ${var2}"
#    echo "ACHEI PDCI_DB_NAME export \"${var1}='${var2}'\" "
#    export "${var1}='${var2}'"
#    ;;
#  PDCI_DB_HOST=*)
#    var1=${line%=*}
#    var2=${line#*=}
#    echo "var1 => ${var1}"
#    echo "var2 => ${var2}"
#    echo "ACHEI PDCI_DB_HOST export \"${var1}='${var2}'\" "
#    echo "PDCI_DB_HOST => ${PDCI_DB_HOST}"
#    unset "${var1}";
#    echo "PDCI_DB_HOST => ${PDCI_DB_HOST}"
#    export "${var1}='${var2}'";
#    echo "PDCI_DB_HOST => ${PDCI_DB_HOST}"
#  ;;
#  esac
#
#
#done < <(grep -v '^ *#' < "${PDCI_FILE_ENV_DOCKER}" )
#
#exit 1
#

#
#export "$(sed 's/\x0D$//' ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//' | sed '/^$/d' | grep -v '^#' | grep 'PDCI_DB_NAME' | xargs)";
#
#export "$(sed 's/\x0D$//' ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//' | sed '/^$/d' | grep -v '^#' | grep 'PDCI_DB_HOST' | xargs)";

#export $(sed 's/\x0D$//' ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//' | sed '/^$/d' | grep -v '^#' | grep 'PDCI_DB_NAME' | xargs);
#
#export $(sed 's/\x0D$//' ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//' | sed '/^$/d' | grep -v '^#' | grep 'PDCI_DB_HOST' | xargs);

if [ "${PDCI_DB_NAME}" = "" ]; then

  echo ""
  echo "===================================== ERRO ==================================="
  echo ""
  echo "A variável PDCI_DB_NAME não foi informada em '${PDCI_FILE_ENV_DOCKER}' ."
  echo ""
  echo "==============================================================================="
  exit 1
fi

if [ "${PDCI_DB_HOST}" = "" ]; then

  echo ""
  echo "===================================== ERRO ==================================="
  echo ""
  echo "A variável PDCI_DB_HOST não foi informada em '${PDCI_FILE_ENV_DOCKER}' ."
  echo ""
  echo "==============================================================================="
  exit 1
fi

echo ""
echo "Deploy do DB '${PDCI_DB_NAME}' no host '${PDCI_DB_HOST}' ."
echo ""

nome_projeto=$(echo "${GIT_URL}" | awk -F'/' '{printf $5}' | sed -e "s/.git//g")

if [ "${nome_projeto}" == "" ]; then
  echo
  echo "***************** ERRO ***********"
  echo "FAVOR INFORMAR O PARAMETRO PDCI_URL_GITLAB_PROJETO"
  echo
  exit 1
fi

### verificar se o banco esta criado caso nao esteja criado o jenkins cria.
# Caso exista o arquivo 000-create-db.sql na pasta o script sera rodado caso contrario ira criar um banco conforme definido neste script do jenkins
#
create_database() {
  vartest=$(

    PGPASSWORD=$password_db_jenkins \
    psql \
    -U $username_db_jenkins \
    -h "${PDCI_DB_HOST}" \
    -p 5432 \
    -t -c \
      " SELECT count(*) FROM pg_database WHERE datname='${PDCI_DB_NAME}'"
  )

  if [ $vartest = 0 ]; then

    if [ "$1" != "" ]; then
# Verifica se existe a palavra drop exite no arquivo.

      DROP=$(grep -i -w "drop" "$1" | xargs);
      DATABASE=$(grep -i -w "database"  "$1" | xargs);

      if [ "${DROP}" != "" ] && [ "${DATABASE}" != "" ]; then
         echo ""
         echo "********* ERRO no script ${1} ************************************************************************"
         echo ""
         echo " Não é permitido usar os termos \"DUMP\" e \"DATABASE\" simultaneamente no mesmo script ;"
         echo ""
         echo "******************************************************************************************************"
         echo ""
         exit 1;

      fi

      #set myvariable '\'somestring\''

      PGPASSWORD=$password_db_jenkins psql \
        -v ON_ERROR_STOP=1 \
        -v PDCI_DB_NAME=${PDCI_DB_NAME} \
        --echo-errors \
        -U $username_db_jenkins \
        -h "${PDCI_DB_HOST}" \
        -p 5432 \
        < "${1}" && \
        create_schema_jenkins_if_not_exists && \
        registra_sql "sql/000-create-db.sql"

    else

  PGPASSWORD=$password_db_jenkins \
  psql \
  -v ON_ERROR_STOP=1 \
  --echo-errors  \
  --username "$username_db_jenkins" \
  -h "${PDCI_DB_HOST}" \
  -p 5432 <<-EOSQL
  DO
  \$DO$
  BEGIN
      IF NOT EXISTS (
              SELECT 1 FROM pg_database WHERE datname='${PDCI_DB_NAME}'
          ) THEN

          CREATE DATABASE ${PDCI_DB_NAME}
                    WITH OWNER = usr_jenkins
                    ENCODING = 'UTF8';
      ELSE
         RAISE INFO 'Base de dados  "%"       [ok]', '${PDCI_DB_NAME}';
      END IF;
  END;
  \$DO$;
EOSQL

      create_schema_jenkins_if_not_exists

    fi

  fi

}

create_schema_jenkins_if_not_exists() {
  (
    echo "
      DO
      \$DO$
      BEGIN
          RAISE INFO '';
          RAISE INFO 'Verica se existe o schema jenkins no DB "${PDCI_DB_NAME}" .';
          RAISE INFO '';
          IF NOT EXISTS (
                  SELECT
                    FROM information_schema.tables
                   WHERE table_schema = 'jenkins'
                     AND table_name = 'controle_versao'
                     AND table_catalog = '${PDCI_DB_NAME}'
              ) THEN


              CREATE SCHEMA IF NOT EXISTS jenkins AUTHORIZATION ${username_db_jenkins};
              COMMENT ON SCHEMA jenkins IS 'Schema do jenkins para controlar as atualizações de sql no banco de dados do ambiente';

              CREATE TABLE jenkins.controle_versao
              (
                  des_tag text  NOT NULL,
                  script_rodado text  NOT NULL,
                  data_inclusao text  NOT NULL DEFAULT now(),
                  ordem serial NOT NULL,
                  sistema text  NOT NULL,
                  username text  NOT NULL,
                  ticket text ,
                  obs text ,
                  CONSTRAINT controle_versao_pk PRIMARY KEY (sistema, des_tag, script_rodado)
              );

              COMMENT ON TABLE jenkins.controle_versao IS
                'Armazena as atualizações ocorridas via a pasta sql do projeto em uma determinada tag no ambiente.
                 e qual script de banco foi rodado';

              CREATE INDEX controle_versao_pk_i  ON jenkins.controle_versao
                    USING btree  (sistema COLLATE pg_catalog.\"default\", script_rodado COLLATE pg_catalog.\"default\");

              ALTER TABLE jenkins.controle_versao OWNER TO ${username_db_jenkins};
              ALTER ROLE ${username_db_jenkins} SET search_path TO jenkins;

              RAISE INFO '';
              RAISE INFO 'Schema jenkins criado com sucesso.';
              RAISE INFO '';

          ELSE
              RAISE INFO 'Schema jenkins        [ok] .';
          END IF;

      END;
      \$DO$;
  "
  ) | PGPASSWORD=$password_db_jenkins psql -v ON_ERROR_STOP=1 -At -h ${PDCI_DB_HOST} -p 5432 -U ${username_db_jenkins} -d ${PDCI_DB_NAME}
}

registra_sql() {
  PGPASSWORD=$password_db_jenkins psql -v ON_ERROR_STOP=1 --username "$username_db_jenkins" -h "${PDCI_DB_HOST}" -p 5432  -d ${PDCI_DB_NAME} <<-EOSQL
  DO
  \$DO$
  BEGIN
        INSERT INTO jenkins.controle_versao(des_tag, script_rodado,sistema,username)
              VALUES ('${NOME}', '$1', '${nome_projeto}','${BUILD_USER}');

         RAISE INFO '';
         RAISE INFO 'SQL registrado no controle de versao $1';
         RAISE INFO '';

  EXCEPTION WHEN OTHERS THEN
    BEGIN

    RAISE EXCEPTION '[save_sql] Erro:  %', SQLERRM;

    END;
  END;
  \$DO$;
EOSQL
}

load_sql() {
  vartest=$(PGPASSWORD=$password_db_jenkins psql -d ${PDCI_DB_NAME} -U $username_db_jenkins -h ${PDCI_DB_HOST} -p 5432 -t -c \
    "SELECT count(*) FROM jenkins.controle_versao WHERE script_rodado = '${1}' AND sistema='${nome_projeto}' OR (script_rodado = '${1}' AND sistema='ADEFINIR' );")

  if [ $vartest = 0 ]; then

    echo "Rodando script :: ${1}"

      DROP=$(grep -i -w "drop" "$1" | xargs);
      DATABASE=$(grep -i -w "database"  "$1" | xargs);

      if [ "${DROP}" != "" ] && [ "${DATABASE}" != "" ]; then
        echo ""
         echo "********* ERRO no script ${1} ************************************************************************"
         echo ""
         echo " Não é permitido usar os termos \"DUMP\" e \"DATABASE\" simultaneamente no mesmo script ;"
         echo ""
         echo "******************************************************************************************************"
         exit 1;
         echo ""
      fi

    PGPASSWORD=$password_db_jenkins psql -d ${PDCI_DB_NAME} -U $username_db_jenkins -h ${PDCI_DB_HOST} -p 5432 -L log_${1}.log -a < ${1} &>log_${1}.out.log

    RODADOSQL=$(cat log_${1}.out.log)

    flag=$(echo $RODADOSQL | awk '{print match($0,"ERROR:")}')

    if [ $flag = 0 ]; then

      PGPASSWORD=$password_db_jenkins psql -d ${PDCI_DB_NAME} -U $username_db_jenkins -h ${PDCI_DB_HOST} -p 5432 -t -c \
      "INSERT INTO jenkins.controle_versao(des_tag, script_rodado,sistema,username)
            VALUES ('${NOME}', '${1}', '${nome_projeto}','${BUILD_USER}');" &>log_${1}.ins.out.log

      REGISTRAATUALIZACAO=$(cat log_${1}.ins.out.log)

      flag2=$(echo $REGISTRAATUALIZACAO | awk '{print match($0,"ERROR:")}')

      if [ $flag2 = 0 ]; then
        echo ""
        echo "#-----------------------------------------#"
        echo "   SQL registrado no controle de versao"
        echo "#-----------------------------------------#"
        echo ""
      else
        echo ""
        echo "#-------------------------------------------#"
        echo "#Erro ao registrar no controle de versao"
        echo "#-------------------------------------------#"
        echo ""
        echo $REGISTRAATUALIZACAO
        exit 1
      fi
    else
      echo ""
      echo "#------------------------------------#"
      echo "FAILLLLLLLLLLLLL"
      echo "#------------------------------------#"
      echo ""
      echo ""
      echo "==============ERRO====================="
      echo $RODADOSQL | awk '{print match($0,"ERROR:")}'
      echo "==============ERRO====================="
      echo ""
      echo ""
      echo ""
      echo $RODADOSQL
      exit 1
    fi
  else
    echo ""
    echo "#---------------------------------------------------------------------------------#"
    echo "#Script ${1} já tinha sido rodado no banco ${PDCI_DB_NAME} do servidor ${PDCI_DB_HOST}   #"
    echo "#---------------------------------------------------------------------------------#"
    echo ""
  fi

}

echo "====================================================================================================="
echo ""
echo "                                Plano de Deploy do DB da pasta sql "
echo ""
echo "====================================================================================================="

if [ -f sql/000-create-db.sql ]; then
  echo ""
  echo "Lendo arquivo script customizado de 'create database' (sql/000-create-db.sql)  no projeto."
  echo ""
  create_database sql/000-create-db.sql
else
  create_database
fi
echo ""
echo "******************************************************************************************************"
echo ""
echo "                                Rotina de deploy do DB da pasta sql " && \
echo ""
echo "******************************************************************************************************"
find  sql/* -name '*.sql' -type f > lista_sql;

(
    echo "SELECT '';
    SELECT 'Criando tabela temporaria com o plano de Deploy de script.';
    SELECT '';
    DROP TABLE IF EXISTS jenkins.tmp_jenkins_lista_sql ;
    CREATE TABLE IF NOT EXISTS jenkins.tmp_jenkins_lista_sql (
     arquivo text
     );
     \COPY jenkins.tmp_jenkins_lista_sql FROM 'lista_sql' DELIMITER ',' CSV ;
     SELECT '';
     SELECT 'Script sql já rodados .';
     SELECT '';
     SELECT
      --concat(public.to_char(data_inclusao::TIMESTAMP, 'dd/mm/yyyy HH24:MI:SS'::text),'  Usuário: ',username, ' script: ', script_rodado , '  branch/tag: ' , des_tag , '.')
      script_rodado
      FROM jenkins.tmp_jenkins_lista_sql
      INNER JOIN jenkins.controle_versao ON controle_versao.script_rodado = tmp_jenkins_lista_sql.arquivo
     ORDER BY arquivo;
  ";
  ) | PGPASSWORD=$password_db_jenkins psql -v ON_ERROR_STOP=1 -t -h ${PDCI_DB_HOST} -p 5432 -U ${username_db_jenkins} -d ${PDCI_DB_NAME} && \
\
    PGPASSWORD=$password_db_jenkins psql -v ON_ERROR_STOP=1  -U $username_db_jenkins -h ${PDCI_DB_HOST} -p 5432 -At  -d ${PDCI_DB_NAME} -c "SELECT arquivo
      FROM jenkins.tmp_jenkins_lista_sql
      LEFT JOIN jenkins.controle_versao ON controle_versao.script_rodado = tmp_jenkins_lista_sql.arquivo
      WHERE controle_versao.script_rodado is null
     ORDER BY arquivo;" > lista_pendente && \
\
while IFS= read -r line
do
  if [ "$line" != "" ]; then
    DROP=$(grep -i -w "drop" "$line" | xargs);
    DATABASE=$(grep -i -w "database"  "$line" | xargs);


    if [ "${DROP}" != "" ] && [ "${DATABASE}" != "" ]; then
       echo ""
       echo "********* ERRO no script ${line} ************************************************************************"
         echo ""
         echo " Não é permitido usar os termos \"DUMP\" e \"DATABASE\" simultaneamente no mesmo script ;"
         echo ""
         echo "******************************************************************************************************"
       exit 1;
       echo ""
    fi
    echo ""
    echo "Executando script : $line."
    echo ""
    (
     echo "\i $line;
     INSERT INTO jenkins.controle_versao(des_tag, script_rodado,sistema,username)
            VALUES ('${NOME}', '${line}', '${nome_projeto}','${BUILD_USER}');
            "
    ) | PGPASSWORD=$password_db_jenkins psql -v ON_ERROR_STOP=1 ${PDCI_DISPLAY_SQL} -At -h ${PDCI_DB_HOST} -p 5432 -U ${username_db_jenkins} -d ${PDCI_DB_NAME}
  fi;
done < <(grep -v '^ *#' < lista_pendente)

PGPASSWORD=$password_db_jenkins psql -v ON_ERROR_STOP=1  -U $username_db_jenkins -h ${PDCI_DB_HOST} -p 5432 -At  -d ${PDCI_DB_NAME} -c \
"ALTER DATABASE ${PDCI_DB_NAME}  OWNER TO postgres;"

##!/bin/bash
#
#echo "${GIT_BRANCH}" > tmp.sh
#sed -i "s/origin\///g" tmp.sh
#rm -rf script_out
#mkdir script_out
#rm -rf log_sql
#mkdir log_sql
#NOME=$(cat tmp.sh)
#
#dbname=db_tcti_sisicmbio
#servidor=dbtcti01.icmbio.gov.br
#
#
#nome_projeto=$(echo ${PDCI_URL_GITLAB_PROJETO} | awk -F'/' '{printf $5}'  | sed -e "s/.git//g" )
#
#if [ "${nome_projeto}" == ""  ]; then
#echo
#echo "***************** ERRO ***********"
#echo "FAVOR INFORMAR O PARAMETRO PDCI_URL_GITLAB_PROJETO"
#echo
#exit 1
#fi
#
#
#
#PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c "SELECT count(*) FROM information_schema.tables  WHERE table_schema='jenkins' and table_name = 'controle_versao';"
#
#vartest=$(PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c "SELECT count(*) FROM information_schema.tables  WHERE table_schema='jenkins' and table_name = 'controle_versao';")
#
##echo "vartest=> ${vartest}"
#
#if [ $vartest -eq 0 ]; then
#
#  echo ""
#  echo "**************************************"
#  echo "  Creando estrutura do jenkins no bd "
#  echo "**************************************"
#  echo ""
#  PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c  "CREATE SCHEMA IF NOT EXISTS jenkins AUTHORIZATION ${username_db_jenkins};"
#  PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c  "COMMENT ON SCHEMA jenkins IS 'Tabela responsavel em controlar as atualizações de sql no banco de dados do ambiente';"
#  PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c  "CREATE TABLE jenkins.controle_versao (  des_tag text NOT NULL,  script_rodado text NOT NULL,  data_inclusao text NOT NULL DEFAULT now(),  ordem serial NOT NULL,  sistema text NOTNULL, -- tabela com o nome do projeto  CONSTRAINT controle_versao_pk PRIMARY KEY (sistema, script_rodado)) WITH (  OIDS=FALSE ); GRANT ALL ON TABLE jenkins.controle_versao TO usr_jenkins; COMMENT ON TABLE jenkins.controle_versao IS 'controle das atualizações via script realizada pela tag no ambiente e qual script de banco foi rodado'; COMMENT ON COLUMN jenkins.controle_versao.sistema IS 'tabela com o nome do projeto'; CREATE INDEX controle_versao_pk_i  ON jenkins.controle_versao  USING btree  (sistema COLLATE pg_catalog.\"default\", script_rodado COLLATE pg_catalog.\"default\");"
#
#  PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c  "ALTER TABLE jenkins.controle_versao OWNER TO ${username_db_jenkins};"
#
#  PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c "ALTER ROLE ${username_db_jenkins} SET search_path TO jenkins;"
#
#fi
#
#for entry in sql/*
#do
#
#  if [ -f "$entry" ]; then
#
#    vartest=$(PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c "SELECT count(*) FROM jenkins.controle_versao WHERE script_rodado = '$entry' AND sistema='${nome_projeto}' OR (script_rodado = '$entry' AND sistema='ADEFINIR' );")
#
#    if [ $vartest = 0 ]; then
#
#        echo "Rodando script :: $entry"
#
#        PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins -h  $servidor -p 5432 -L log_$entry.log  -a < $entry &> log_$entry.out.log
#
#        RODADOSQL=$(cat log_$entry.out.log)
#
#        flag=`echo $RODADOSQL|awk '{print match($0,"ERROR:")}'`
#
#        if [ $flag = 0 ]; then
#
#            PGPASSWORD=$password_db_jenkins  psql -d $dbname -U $username_db_jenkins -h $servidor -p 5432 -t -c "INSERT INTO jenkins.controle_versao(des_tag, script_rodado,sistema,username)VALUES ('${NOME}', '$entry', '${nome_projeto}','${BUILD_USER}');" &> log_$entry.ins.out.log
#
#
#            REGISTRAATUALIZACAO=$(cat log_$entry.ins.out.log)
#
#            flag2=`echo $REGISTRAATUALIZACAO|awk '{print match($0,"ERROR:")}'`
#
#            if [ $flag2 = 0 ]; then
#              echo ""
#              echo "#-----------------------------------------#"
#              echo "   SQL registrado no controle de versao"
#              echo "#-----------------------------------------#"
#              echo ""
#            else
#              echo ""
#              echo "#-------------------------------------------#"
#              echo "#Erro ao registrar no controle de versao"
#              echo "#-------------------------------------------#"
#              echo ""
#              echo $REGISTRAATUALIZACAO
#              exit 1
#            fi
#        else
#        	echo ""
#            echo "#------------------------------------#"
#            echo "FAILLLLLLLLLLLLL"
#            echo "#------------------------------------#"
#            echo ""
#            echo ""
#            echo "==============ERRO====================="
#            echo $RODADOSQL|awk '{print match($0,"ERROR:")}'
#            echo "==============ERRO====================="
#            echo ""
#            echo ""
#            echo ""
#            echo $RODADOSQL
#            exit 1
#        fi
#     else
#       echo ""
#       echo "#---------------------------------------------------------------------------------#"
#       echo "#Script $entry já tinha sido rodado no banco ${dbname} do servidor ${servidor}    #"
#       echo "#---------------------------------------------------------------------------------#"
#       echo ""
#    fi
#  else
#	echo "DIRETORIO"
#  fi
#done
#
#
#echo ""
#echo "***************************************************************"
#echo ""
#echo "  Normalizando URL's de sistemas cadastrados no sicae "
#echo ""
#echo "***************************************************************"
#echo ""
#
#echo ""
#echo "      1 Normalizando URL's com CDN no formato antigo. "
#echo ""
#
#PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c  "SELECT tx_url FROM sicae.sistema WHERE tx_url in ('https://canie.sisicmbio.icmbio.gov.br/','https://sige.sisicmbio.icmbio.gov.br','https://infoconv.sisicmbio.icmbio.gov.br','https://cis.sisicmbio.icmbio.gov.br','https://sismidia.sisicmbio.icmbio.gov.br','https://cairu.sisicmbio.icmbio.gov.br','https://simac.sisicmbio.icmbio.gov.br','https://brigadistas.sisicmbio.icmbio.gov.br','https://sit.sisicmbio.icmbio.gov.br','https://sarr.sisicmbio.icmbio.gov.br','https://sisfamilias.sisicmbio.icmbio.gov.br','https://sgd.sisicmbio.icmbio.gov.br','https://sofia.sisicmbio.icmbio.gov.br','https://capacitacao.sisicmbio.icmbio.gov.br','https://sgdoce.sisicmbio.icmbio.gov.br','https://voluntariado.sisicmbio.icmbio.gov.br','https://portaldabiodiversidade.icmbio.gov.br/portal/sicaeLogin/','https://wiadmin.sisicmbio.icmbio.gov.br','https://inventario.sisicmbio.icmbio.gov.br','https://sisva.sisicmbio.icmbio.gov.br','http://sigeo.icmbio.gov.br','https://sisbio.sisicmbio.icmbio.gov.br','https://sicae.sisicmbio.icmbio.gov.br','https://salve.icmbio.gov.br/salve/login','https://sintax.icmbio.gov.br/sintax/sicaeLogin')
#	  AND tx_url not like 'http%//tcti.%';"
#
#echo ""
#echo ""
#
#PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c  "UPDATE sicae.sistema
#	SET tx_url=replace(tx_url,'://','://tcti.')
#	WHERE tx_url in ('https://canie.sisicmbio.icmbio.gov.br/','https://sige.sisicmbio.icmbio.gov.br','https://infoconv.sisicmbio.icmbio.gov.br','https://cis.sisicmbio.icmbio.gov.br','https://sismidia.sisicmbio.icmbio.gov.br','https://cairu.sisicmbio.icmbio.gov.br','https://simac.sisicmbio.icmbio.gov.br','https://brigadistas.sisicmbio.icmbio.gov.br','https://sit.sisicmbio.icmbio.gov.br','https://sarr.sisicmbio.icmbio.gov.br','https://sisfamilias.sisicmbio.icmbio.gov.br','https://sgd.sisicmbio.icmbio.gov.br','https://sofia.sisicmbio.icmbio.gov.br','https://capacitacao.sisicmbio.icmbio.gov.br','https://sgdoce.sisicmbio.icmbio.gov.br','https://voluntariado.sisicmbio.icmbio.gov.br','https://portaldabiodiversidade.icmbio.gov.br/portal/sicaeLogin/','https://wiadmin.sisicmbio.icmbio.gov.br','https://inventario.sisicmbio.icmbio.gov.br','https://sisva.sisicmbio.icmbio.gov.br','http://sigeo.icmbio.gov.br','https://sisbio.sisicmbio.icmbio.gov.br','https://sicae.sisicmbio.icmbio.gov.br','https://salve.icmbio.gov.br/salve/login','https://sintax.icmbio.gov.br/sintax/sicaeLogin')
#	  AND tx_url not like 'http%//tcti.%';"
#
#echo ""
#echo "      2 Normalizando URL's com CDN no formato novo. "
#echo ""
#
#PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c  "SELECT tx_url FROM sicae.sistema 	WHERE tx_url not in ('https://canie.sisicmbio.icmbio.gov.br/','https://sige.sisicmbio.icmbio.gov.br','https://infoconv.sisicmbio.icmbio.gov.br','https://cis.sisicmbio.icmbio.gov.br','https://sismidia.sisicmbio.icmbio.gov.br','https://cairu.sisicmbio.icmbio.gov.br','https://simac.sisicmbio.icmbio.gov.br','https://brigadistas.sisicmbio.icmbio.gov.br','https://sit.sisicmbio.icmbio.gov.br','https://sarr.sisicmbio.icmbio.gov.br','https://sisfamilias.sisicmbio.icmbio.gov.br','https://sgd.sisicmbio.icmbio.gov.br','https://sofia.sisicmbio.icmbio.gov.br','https://capacitacao.sisicmbio.icmbio.gov.br','https://sgdoce.sisicmbio.icmbio.gov.br','https://voluntariado.sisicmbio.icmbio.gov.br','https://portaldabiodiversidade.icmbio.gov.br/portal/sicaeLogin/','https://wiadmin.sisicmbio.icmbio.gov.br','https://inventario.sisicmbio.icmbio.gov.br','https://sisva.sisicmbio.icmbio.gov.br','http://sigeo.icmbio.gov.br','https://sisbio.sisicmbio.icmbio.gov.br','https://sicae.sisicmbio.icmbio.gov.br','https://salve.icmbio.gov.br/salve/login','https://sintax.icmbio.gov.br/sintax/sicaeLogin')
#	  AND tx_url not like 'http%//tcti%';"
#
#echo ""
#echo ""
#
#PGPASSWORD=$password_db_jenkins psql -d $dbname -U $username_db_jenkins  -h $servidor -p 5432 -t -c  "UPDATE sicae.sistema
#	SET tx_url=replace(tx_url,'://','://tcti')
#	WHERE tx_url not in ('https://canie.sisicmbio.icmbio.gov.br/','https://sige.sisicmbio.icmbio.gov.br','https://infoconv.sisicmbio.icmbio.gov.br','https://cis.sisicmbio.icmbio.gov.br','https://sismidia.sisicmbio.icmbio.gov.br','https://cairu.sisicmbio.icmbio.gov.br','https://simac.sisicmbio.icmbio.gov.br','https://brigadistas.sisicmbio.icmbio.gov.br','https://sit.sisicmbio.icmbio.gov.br','https://sarr.sisicmbio.icmbio.gov.br','https://sisfamilias.sisicmbio.icmbio.gov.br','https://sgd.sisicmbio.icmbio.gov.br','https://sofia.sisicmbio.icmbio.gov.br','https://capacitacao.sisicmbio.icmbio.gov.br','https://sgdoce.sisicmbio.icmbio.gov.br','https://voluntariado.sisicmbio.icmbio.gov.br','https://portaldabiodiversidade.icmbio.gov.br/portal/sicaeLogin/','https://wiadmin.sisicmbio.icmbio.gov.br','https://inventario.sisicmbio.icmbio.gov.br','https://sisva.sisicmbio.icmbio.gov.br','http://sigeo.icmbio.gov.br','https://sisbio.sisicmbio.icmbio.gov.br','https://sicae.sisicmbio.icmbio.gov.br','https://salve.icmbio.gov.br/salve/login','https://sintax.icmbio.gov.br/sintax/sicaeLogin')
#	  AND tx_url not like 'http%//tcti%';"
#
#echo ""
#echo ""
