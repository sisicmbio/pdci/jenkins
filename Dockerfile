#FROM alpine:latest AS bkp
#RUN mkdir /bkp
#COPY .  /bkp/

FROM registry.gitlab.com/sisicmbio/pdci/image_file_dump:empty AS sql
ENV APP_PROJETO "jenkins"
COPY sql  /sql/
ENTRYPOINT ["/sbin/tini", "--","/bin/pdci_upd_sql.sh"]

FROM jenkins/jenkins:2.366-alpine as oficialimage

c
USER root
COPY src/executors.groovy /usr/share/jenkins/ref/init.groovy.d/executors.groovy
COPY src/plugins.txt /usr/share/jenkins/ref/plugins.txt
#COPY src/config.xml.override /usr/share/jenkins/ref/config.xml.override

#RUN ls -la /usr/local/bin/install-plugins.sh
#RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
#RUN cat jenkins.install.UpgradeWizard.state
RUN echo "2.0" > /usr/share/jenkins/ref/jenkins.install.UpgradeWizard.state

RUN apk add --update --no-cache --virtual .fetch-sudo \
    git \
    openssh-client \
    curl \
    unzip \
    bash \
    ttf-dejavu \
    coreutils \
    jq \
    ca-certificates \
    openssl \
    wget \
    bash \
    subversion \
    libltdl \
    sudo \
    shadow \
    libltdl \
    util-linux \
    pciutils \
    usbutils \
    coreutils \
    binutils \
    findutils \
    grep \
    bash \
    bash-doc \
    bash-completion \
    tar \
    gzip \
    gettext \
    && rm -rf /var/lib/apt/lists/*
RUN echo "jenkins ALL=NOPASSWD: ALL" >> /etc/sudoers

#Adicionando Docker
#The rc-update tool is a part of the openrc package
#
# apk add openrc
# https://github.com/just-containers/s6-overlay
#&   & apk add --update --no-cache py-pip python-dev libffi-dev openssl-dev gcc libc-dev  make \
# Dependencia
#
# https://wiki.alpinelinux.org/wiki/Docker

RUN apk add --update --no-cache docker \
    && apk add --update --no-cache openrc \
    && rc-update add docker boot \
    #&& apk add --update --no-cache py-pip python-dev libffi-dev openssl-dev gcc libc-dev  make \
    #&& sudo pip install docker-compose \
    #&& docker-compose -v \
    && apk add --update --no-cache docker-compose \
    #Instalar o client do postgresql
    && apk add --update --no-cache postgresql-client \
    #&& apk add --update --no-cache nodejs \ #não identifiquei quando usamos o nodejs por isso desabilitei \
    && rm -rf /var/lib/apt/lists/* \
    && wget -q -O /usr/local/bin/yq $(wget -q -O - https://api.github.com/repos/mikefarah/yq/releases/latest | jq -r '.assets[] | select(.name == "yq_linux_amd64") | .browser_download_url') \
    && chmod +x /usr/local/bin/yq \
    && yq -V \
    && cd /usr/local/bin/ \
    && curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.16.0/bin/linux/amd64/kubectl \
    && chmod 777 "/usr/local/bin/kubectl" \
    && chmod +x  "/usr/local/bin/kubectl" \
    && cd - \
    && curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 \
    && chmod 700 get_helm.sh \
    && ./get_helm.sh \
    && rm -rf /root/.cache



#criacao dos jobs de exemplos previstos no PDCI para o JENKINS
COPY src/pdci_install_jobs.sh /pdci_install_jobs.sh
COPY src/pdci_get_jenkins_prd.sh /pdci_get_jenkins_prd.sh
COPY src/pdci_clean_workspace_jobs.sh /pdci_clean_workspace_jobs.sh
COPY src/pdci_bkp_jenkins_home.sh /pdci_bkp_jenkins_home.sh

RUN chmod +x /pdci_install_jobs.sh && \
    chmod +x /pdci_get_jenkins_prd.sh && \
    chmod +x /pdci_clean_workspace_jobs.sh && \
    chmod +x /pdci_bkp_jenkins_home.sh && \
    rm -rf pdci-jobs-jenkins && \
    /pdci_install_jobs.sh


USER jenkins

#FROM registry.gitlab.com/sisicmbio/infra/alpine-3.11_php7_apache2:develop AS production
#
##Tag develop
#FROM production AS develop
#
##Tag build
#FROM develop AS build
#
##Tag testing
#FROM develop AS testing