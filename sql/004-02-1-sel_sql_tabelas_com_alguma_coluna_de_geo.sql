﻿with sel AS (
	select f_table_catalog
	       , f_table_schema
	       , f_table_name
	       , f_geometry_column
	       , srid
	       , type
	 from geometry_columns
 ), sel_obj_geo AS (

  select * 
    from information_schema.role_column_grants 
   inner join sel ON sel.f_table_catalog = role_column_grants.table_catalog 
     AND sel.f_table_schema  = role_column_grants.table_schema
     AND sel.f_table_name = role_column_grants.table_name
     AND sel.f_geometry_column = role_column_grants.column_name
 )select
--grantor, 
grantee,table_catalog,table_schema, table_name, array_agg(DISTINCT "column_name"::text) as agg_column_name, array_agg(DISTINCT "privilege_type"::text) agg_privilege_type , array_agg(DISTINCT "type"::text) as agg_type
--array_agg(column_name::text) OVER (PARTITION BY grantor, grantee,table_catalog,table_schema, table_name, column_name) 
 from 
 sel_obj_geo
 WHERE grantee not in ('postgres','usr_jenkins')
 --AND grantee in ('usr_lafsisbio')
 GROUP BY 
 --grantor, 
 grantee,table_catalog,table_schema, table_name
 ORDER BY grantee
 , sel_obj_geo.table_catalog 
 , sel_obj_geo.table_schema
 , sel_obj_geo.table_name
 --, sel_obj_geo.column_name
 
 

 /**
 select f_table_catalog
       , f_table_schema
       , f_table_name
       , agg_f_geometry_column
       , agg_srid
       , agg_type
       , role_column_grants.*
 from sel
inner join information_schema.role_column_grants 
        ON role_column_grants.table_catalog =sel.f_table_catalog  
       AND role_column_grants.table_schema =sel.f_table_schema 
       AND role_column_grants.table_name =sel.f_table_name 
       AND role_column_grants.table_name =sel.f_table_name 
order by f_table_catalog
       , f_table_schema
       , f_table_name
 --select * from information_schema.role_column_grants
 **/

 --select * from information_schema.role_column_grants 