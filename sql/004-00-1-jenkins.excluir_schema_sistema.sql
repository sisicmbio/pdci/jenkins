CREATE OR REPLACE FUNCTION jenkins.excluir_schema_sistema(p_no_projeto text,p_sg_sistema text[], p_no_schema text

)
  RETURNS text AS
$BODY$
DECLARE
    v_texto text; 
  
BEGIN

if p_no_projeto is null then
	return 'Favor informar o nome do sistema no gitlab. exemplo: app_lafsisbio';
end if;

if p_sg_sistema is null then
	return 'Favor informar a sigla do sistema no sicae como array exemplo: ''{"LAFSisbio"}'' ';
end if;

if p_no_schema is null then
	return 'Favor informar o schema no banco do sistema. Exemplo: lafsisbio ';
end if;

SELECT sicae.pdci_del_sistema_if_exists(p_sg_sistema) into v_texto;

DELETE FROM jenkins.controle_versao where sistema = p_no_projeto;


execute 'DROP SCHEMA IF EXISTS '||p_no_schema||' CASCADE;'; 

return 'Realizado com sucesso.';
	  
EXCEPTION WHEN OTHERS THEN 
  BEGIN 
  
  RAISE EXCEPTION 'Erro: %', SQLERRM; 
  
  END;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE;
GRANT EXECUTE ON FUNCTION jenkins.excluir_schema_sistema(text,text[],text) TO usr_jenkins WITH GRANT OPTION;
GRANT EXECUTE ON FUNCTION jenkins.excluir_schema_sistema(text,text[],text) TO usr_lafsisbio;
COMMENT ON FUNCTION jenkins.excluir_schema_sistema(text,text[],text) IS 'Função para excluir o schema do sistema, informação de atualizacao de banco de dados no jenkins, e o cadastro do sistema do sicae.';

--select jenkins.excluir_schema_sistema('app_lafsisbio','{"LAFSisbio"}', 'lafsisbio');
