﻿select 
grantor,grantee, table_catalog, table_schema, 
 (
 CASE 
  WHEN grantee = 'usr_libcorp' THEN 'corporativo' 
  ELSE replace(grantee,'usr_','')
 END
 ) as user_grant_owner ,
'TABLE' as objeto,
'format(''REVOKE ALL ON TABLE %.% FROM %; GRANT % ON TABLE %.% TO %'',sel.table_schema,sel.table_name,sel.grantee,array_to_string(sel.agg_privilege_type_permanece, '',''),sel.table_schema,sel.table_name,sel.grantee)' comando,
--concat('REVOKE ALL ON TABLE ',sel.table_schema,'.',sel.table_name,' FROM ',sel.grantee,';',' GRANT ',array_to_string(sel.agg_privilege_type_permanece, ',') ,' ON TABLE ',sel.table_schema,'.',sel.table_name,' TO ',sel.grantee) as comando_sql,
table_name::text as table_name,
array_agg(DISTINCT privilege_type::text) as agg_privilege_type_atual,  
        array_agg(DISTINCT privilege_type::text) FILTER (WHERE privilege_type in ('DELETE',
          'INSERT',
          'UPDATE',
          'TRUNCATE')) as agg_privilege_type_remove,
        array_agg(DISTINCT privilege_type::text) FILTER (WHERE privilege_type not in ('DELETE',
          'INSERT',
          'UPDATE',
          'TRUNCATE')
        ) as agg_privilege_type_permanece
   from information_schema.role_table_grants 
   inner join pg_tables on pg_tables.tablename=role_table_grants.table_name and pg_tables.schemaname=role_table_grants.table_schema
   where  table_catalog in ('db_sisicmbio','db_dev_cotec' )
    -- and table_schema='corporativo' 
     and grantee not in ( 
                        'postgres',
                        'usr_jenkins'--,
                       -- 'auditoria', -- usuario que possui grant nas tabelas do corporativo.
                       -- 'usr_sicae', -- Verifiquei que as permissões estão corretas com o corporativo
                       -- 'usr_sica' --Não é mais necessario ter permissão alguma a este usuário. Porque nao existe mais o sistema sica (renomear este usuário de usr_sica para del_usr_sica, temporario para verificar algum integracao caso nao de nenhum problema em uma semana pode tirar excluir o usuario)
                       )
group by grantor,grantee, table_catalog, table_schema, table_name
order by grantor,grantee, table_catalog, table_schema, table_name 