drop function if exists jenkins.disable_trigger();
CREATE OR REPLACE FUNCTION jenkins.disable_trigger()
    RETURNS text AS

$BODY$
DECLARE
    v_sql text;
BEGIN

    SELECT
--table_schema
--, table_name
--,
array_to_string(array_agg('ALTER TABLE "'||table_schema||'"."'||table_name||'" DISABLE TRIGGER ALL;'),'') as sql into v_sql
    FROM information_schema.tables  WHERE table_catalog='db_dev_cotec'
                                      and table_schema
            not in ('tiger','topology','information_schema','pg_catalog','public')
                                      AND table_type = 'BASE TABLE';

    EXECUTE v_sql;

    return 'Todas as TRIGGER''s Desabilitadas.';


END;
$BODY$
    LANGUAGE plpgsql;
ALTER FUNCTION jenkins.disable_trigger()
    OWNER TO usr_jenkins;
--select jenkins.disable_trigger();

drop function if exists jenkins.enable_trigger();
CREATE OR REPLACE FUNCTION jenkins.enable_trigger()
    RETURNS text AS

$BODY$
DECLARE
    v_sql text;
BEGIN

    SELECT
--table_schema
--, table_name
--,
array_to_string(array_agg('ALTER TABLE "'||table_schema||'"."'||table_name||'" ENABLE TRIGGER ALL;'),'') as sql into v_sql
    FROM information_schema.tables  WHERE table_catalog='db_dev_cotec'
                                      and table_schema
            not in ('tiger','topology','information_schema','pg_catalog','public')
                                      AND table_type = 'BASE TABLE';

    EXECUTE v_sql;

    return 'Todas as TRIGGER''s Habilitadas.';
END;
$BODY$
    LANGUAGE plpgsql;
ALTER FUNCTION jenkins.enable_trigger()
    OWNER TO usr_jenkins;

--select jenkins.enable_trigger();

REFRESH MATERIALIZED VIEW taxonomia.mv_taxonomia WITH NO DATA;
--REFRESH MATERIALIZED VIEW taxonomia.mv_ipt_sisbio_ocorrencia WITH NO DATA;
--REFRESH MATERIALIZED VIEW taxonomia.mv_arvore_taxon_em_uso WITH NO DATA;
--REFRESH MATERIALIZED VIEW taxonomia.mv_discovery_fk_taxon WITH NO DATA;