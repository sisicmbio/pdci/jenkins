﻿with sel as (
 select (
 CASE 
  WHEN grantee = 'usr_libcorp' THEN 'corporativo' 
  ELSE replace(grantee,'usr_','')
 END
 ) as schema_permitido_para_usuario
 ,grantor,grantee, table_catalog, table_schema, table_name ,
array_agg(privilege_type::text order by grantor,grantee, table_catalog, table_schema, table_name, privilege_type) as agg_privilege_type_atual,  
        array_agg(privilege_type::text order by grantor,grantee, table_catalog, table_schema, table_name, privilege_type) FILTER (WHERE privilege_type in ('DELETE',
          'INSERT',
          'UPDATE',
          'TRUNCATE')) as agg_privilege_type_remove,
        array_agg(privilege_type::text order by grantor,grantee, table_catalog, table_schema, table_name, privilege_type) FILTER (WHERE privilege_type not in ('DELETE',
          'INSERT',
          'UPDATE',
          'TRUNCATE')
        ) as agg_privilege_type_permanece, 
        string_agg(is_grantable, ',' order by grantor,grantee, table_catalog, table_schema, table_name, privilege_type) as agg_is_grantable_atual , 
        string_agg(with_hierarchy, ',' order by grantor,grantee, table_catalog, table_schema, table_name, privilege_type)as agg_with_hierarchy_atual
   from information_schema.role_table_grants 
   inner join pg_tables on pg_tables.tablename=role_table_grants.table_name and pg_tables.schemaname=role_table_grants.table_schema
   where  table_catalog in ('db_sisicmbio','db_dev_cotec' )
    -- and table_schema='corporativo' 
     and grantee not in ( 
                        'postgres',
                        'usr_jenkins'--,
                       -- 'usr_libcorp', -- usuario que possui grant nas tabelas do corporativo.
                       -- 'usr_sicae', -- Verifiquei que as permissões estão corretas com o corporativo
                       -- 'usr_sica' --Não é mais necessario ter permissão alguma a este usuário. Porque nao existe mais o sistema sica (renomear este usuário de usr_sica para del_usr_sica, temporario para verificar algum integracao caso nao de nenhum problema em uma semana pode tirar excluir o usuario)
                       )
 group by grantor,grantee, table_catalog, table_schema, table_name
    order by grantor,grantee, table_catalog, table_schema, table_name 
)
select 

--concat('REVOKE ALL ON TABLE ',sel.table_schema,'.',sel.table_name,' FROM ',sel.grantee,';',' GRANT ',array_to_string(sel.agg_privilege_type_permanece, ',') ,' ON TABLE ',sel.table_schema,'.',sel.table_name,' TO ',sel.grantee) as comando_sql,

--*
  from sel
  WHERE agg_privilege_type_remove is not null -- Lista apenas as tabelas que possuem permissão a mais do que a padrao permitida que é apenas o proprio usuário do schema pode ter permissão de INSERT, DELETE,TRUNCATE em seu schema
    AND sel.schema_permitido_para_usuario  <> sel.table_schema -- verificar apenas as permissões do usuário para os schema diferentes do esquema dele. 
    --AND sel.grantee = 'usr_sisbio'
  --  criar a tabela com estas escessoes e ir removendo aos poucos
    

    -- Criar uma tabela no jenkins para informar qual o usuario pertence ao schema como owner. Isso porque o jenkins e owner de todos os objetos para poder permitir alteracao de grant conformo o plano de seguranca dos usuario seja por definição padrão ou pela excessão;
--"INSERT,SELECT" ALL USER "auditoria";"trilha_auditoria"
--"auditoria";"trilha_auditoria";"INSERT,SELECT"
--"auditoria";"trilha_metadata";"INSERT,SELECT"
--"auditoria";"trilha_auditoria";"INSERT,SELECT"
--"auditoria";"trilha_metadata";"INSERT,SELECT"
--"auditoria";"trilha_auditoria";"INSERT,SELECT"
--"auditoria";"trilha_metadata";"INSERT,SELECT"
--"auditoria";"trilha_auditoria";"INSERT,SELECT"
--"auditoria";"trilha_metadata";"INSERT,SELECT"
--"auditoria";"trilha_auditoria";"INSERT,SELECT"
--"auditoria";"trilha_metadata";"INSERT,SELECT"
--"auditoria";"trilha_auditoria";"INSERT,SELECT"
--"auditoria";"trilha_metadata";"INSERT,SELECT"
--"auditoria";"trilha_auditoria";"INSERT,SELECT"
--"auditoria";"trilha_metadata";"INSERT,SELECT"

  