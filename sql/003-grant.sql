GRANT ALL ON SCHEMA jenkins TO usr_jenkins;
--GRANT ALL ON SCHEMA jenkins TO PUBLIC;
--alter role usr_jenkins set search_path = jenkins, public, postgis ;

GRANT ALL ON all tables in SCHEMA jenkins TO usr_jenkins ;
GRANT ALL ON all functions in SCHEMA jenkins TO usr_jenkins;
GRANT ALL ON all sequences in SCHEMA jenkins TO usr_jenkins;
--
-- PostgreSQL database dump complete
--

