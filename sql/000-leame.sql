-- Arquivo apenas de orientacao
-- Nesta pasta contera os sql que serão lidos em ordem de numeração pelo processo de deploy realizado pelo Jenkins
-- Nomeclatura de arquivo 000-Descricao_da_operacao
-- Proibido utilizar espacos e caracters especiais
-- Os logs de execução estarão no diretótio logs do worspace do job de atualização dos sql do projeto. O
--   O nome do job de atualização foi padronizado com o final *-upd-sql-em-DEV-db para o ambiente DEV
-- Dentro do banco de dados possui um esquema jenkins que possui uma tabela chamada controle_de_versao.
--   Esta tabela faz parte do processo do jenkins de saber quais os sql já foram rodados no banco
-- Mesmo que as frameworks mais mordernas utilizam a funcao de "migration" para realizar este processo de atualização, o PDCI
--   adotou a politica de não usar este recurso em função que cada framework realiza de forma diferente este processo e visando o processo de qualidade
--   onde esta previsto que o DBA e/ou AD e responsavel em verificar se as regras referente ao bando de dados estão sendo seguidas;

CREATE SCHEMA IF NOT EXISTS jenkins
    AUTHORIZATION usr_jenkins;

COMMENT ON SCHEMA jenkins
    IS 'SCHEMA responsavel em controlar as atualizações de sql no banco de dados do ambiente atraves do deploy do jenkins';

GRANT ALL ON SCHEMA jenkins TO usr_jenkins WITH GRANT OPTION;



CREATE TABLE IF NOT EXISTS jenkins.controle_versao
(
    des_tag text COLLATE pg_catalog."default" NOT NULL,
    script_rodado text COLLATE pg_catalog."default" NOT NULL,
    data_inclusao text COLLATE pg_catalog."default" NOT NULL DEFAULT now(),
    ordem integer ,
    sistema text COLLATE pg_catalog."default" NOT NULL,
    username text COLLATE pg_catalog."default" NOT NULL,
    ticket text COLLATE pg_catalog."default",
    obs text COLLATE pg_catalog."default",
    CONSTRAINT controle_versao_pk PRIMARY KEY (sistema, script_rodado)
)
    WITH (
        OIDS = FALSE
    )
    TABLESPACE pg_default;

ALTER TABLE IF EXISTS jenkins.controle_versao
    OWNER to usr_jenkins;

GRANT ALL ON TABLE jenkins.controle_versao TO usr_jenkins WITH GRANT OPTION;

COMMENT ON TABLE jenkins.controle_versao
    IS 'controle das atualizações via script realizada pela tag no ambiente e qual script de banco foi rodado';

COMMENT ON COLUMN jenkins.controle_versao.sistema
    IS 'tabela com o nome do projeto';

COMMENT ON COLUMN jenkins.controle_versao.username
    IS 'Usuário logado no  jenkins que fez o deploy no banco de dados';

COMMENT ON COLUMN jenkins.controle_versao.ticket
    IS 'Número do chamado da infra';
-- Index: controle_versao_pk_i

-- DROP INDEX IF EXISTS jenkins.controle_versao_pk_i;

CREATE INDEX IF NOT EXISTS controle_versao_pk_i
    ON jenkins.controle_versao USING btree
        (sistema COLLATE pg_catalog."default" ASC NULLS LAST, script_rodado COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;-- Database: db_dspace6

CREATE SEQUENCE IF NOT EXISTS jenkins.controle_versao_ordem_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1
    OWNED BY controle_versao.ordem;

ALTER SEQUENCE jenkins.controle_versao_ordem_seq
    OWNER TO usr_jenkins;

GRANT ALL ON SEQUENCE jenkins.controle_versao_ordem_seq TO usr_jenkins WITH GRANT OPTION;

ALTER TABLE IF EXISTS jenkins.controle_versao
    ALTER COLUMN ordem SET DEFAULT nextval('controle_versao_ordem_seq'::regclass);

ALTER TABLE IF EXISTS jenkins.controle_versao
    ALTER COLUMN ordem SET NOT NULL;