DO
$$
BEGIN
 IF NOT EXISTS (
     SELECT                       -- SELECT list can stay empty for this
     FROM   pg_catalog.pg_roles
     WHERE  rolname = 'usr_jenkins') THEN

   create user usr_jenkins with password 'usr_jenkins';
 END IF;

--  IF NOT  EXISTS (
--      SELECT -- SELECT list can stay empty for this
--      FROM information_schema.tables  WHERE table_schema='jenkins'
--  )
--  THEN
--   CREATE SCHEMA IF NOT EXISTS jenkins;
--
--  END IF;
 CREATE SCHEMA IF NOT EXISTS jenkins;

 grant all on all tables in schema jenkins to usr_jenkins;
 grant usage on all sequences in schema jenkins to usr_jenkins;
 grant execute on all functions in schema jenkins to usr_jenkins;

 grant all on all tables in schema jenkins to usr_jenkins;
 grant usage on all sequences in schema jenkins to usr_jenkins;
 grant execute on all functions in schema jenkins to usr_jenkins;

END;
$$;
