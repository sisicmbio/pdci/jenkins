#!/bin/sh
set -e
for i in `ls  /var/jenkins_home/workspace/`
 do
  echo " realizando limpeza no workspace  /var/jenkins_home/workspace/$i "
  rm -rf /var/jenkins_home/workspace/$i/*
done