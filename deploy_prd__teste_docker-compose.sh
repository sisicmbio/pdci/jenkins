#!/usr/bin/env bash

usage()
{
    echo "========================================================================================================"
    echo "Selecione em qual ambiente ira instalar:                           "
    echo ""
    echo "      -prd   | prd                 Ambiente PRD - Ambiente local de Produção           "
    echo "========================================================================================================"
}

case $1 in
    -prd| prd )
        export PDCI_FILE_ENV_DOCKER=./.env.docker.prd
        export PDCI_PROJECT_NAME=pdci_teste_production
        export PDCI_DOCKER_HOST_API=" -H tcp://10.197.32.76:2375 "
    ;;
    -h | --help )
        usage
        exit
    ;;
    * )
        usage
        exit 1
esac


echo
echo
echo "Informe seu login e senha para o registry.gitlab.com:"
echo
read -p 'Username: ' PDCI_CI_REGISTRY_USER
read -sp 'Password: ' PDCI_CI_REGISTRY_PASSWORD
echo
echo "Realizando login com  ${PDCI_CI_REGISTRY_USER} em registry.gitlab.com"
echo
docker logout

#echo "${PDCI_CI_REGISTRY_PASSWORD}" | docker   login --username ${PDCI_CI_REGISTRY_USER} --password-stdin  registry.gitlab.com


rm -rf ./.env

export $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_VIRTUAL_HOST' | xargs)

export $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_REGISTRY' | xargs)


#
#
###################################################################################
### DEPLOY DE SERVICO DE APPLICACAO E BANCO DE DADOS CONFORME BRANCH OF DEVELOP
###################################################################################
##
## LER AS VARIAVEIS DO arquivo ${PDCI_FILE_ENV_DOCKER} QUE possui o VIRTUAL_HOST PARA QUE POSSAM SER LIDAS NO DOCKER-COMPOSER
##convercao de CRLF para LF =>  sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d'
##ignorar todas as linhas comentadas => grep -v '^#'
##Deletar todas as linhas em branco =>  sed '/^$/d'
#
export $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_VIRTUAL_HOST' | xargs)

export $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_REGISTRY' | xargs)

export $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep 'pdci_domain_port_registry' | xargs)

docker-compose  ${PDCI_DOCKER_HOST_API} -p ${PDCI_PROJECT_NAME:-dev} -f docker-compose_teste_prd.yml config
docker-compose  ${PDCI_DOCKER_HOST_API} -p ${PDCI_PROJECT_NAME:-dev} -f docker-compose_teste_prd.yml pull
docker-compose  ${PDCI_DOCKER_HOST_API} -p ${PDCI_PROJECT_NAME:-dev} -f docker-compose_teste_prd.yml rm -f -s -v

#docker  volume rm  ${PDCI_PROJECT_NAME:-dev}_jenkins_DocumentRoot -f

docker-compose  ${PDCI_DOCKER_HOST_API} -p ${PDCI_PROJECT_NAME:-dev} -f docker-compose_teste_prd.yml up -d
#docker-compose  ${PDCI_DOCKER_HOST_API} -p ${PDCI_PROJECT_NAME:-dev} -f docker-compose_teste_prd.yml logs -f jenkins_sql
docker-compose  ${PDCI_DOCKER_HOST_API} -p ${PDCI_PROJECT_NAME:-dev} -f docker-compose_teste_prd.yml ps

unset $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_VIRTUAL_HOST' | sed -E 's/(.*)=.*/\1/' | xargs)

unset $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_REGISTRY' | sed -E 's/(.*)=.*/\1/' | xargs)

#docker -H 10.197.32.76 exec -ti -u root pdci_teste_production_jenkins_novo_1 bash -c "cp -Rv /var/dump/jenkins/* /var/jenkins_home/"
#docker -H 10.197.32.76 exec -ti -u root pdci_teste_production_jenkins_novo_1 bash -c "chown jenkins:jenkins -R /var/jenkins_home/"
#docker -H 10.197.32.76 restart pdci_teste_production_jenkins_novo_1
#docker -H 10.197.32.76 logs pdci_teste_production_jenkins_novo_1 -f