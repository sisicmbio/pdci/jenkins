#!/bin/sh

#
#Script para versinar os arquivos de configuração para construção de uma imagem com as ultimas configurações.
#
#

recuperar_arquivos_de_configuracao_prd () {

	echo ""
    echo " Descobrir container jenkins em tcp://10.197.32.76:2375 ."
    echo ""
    echo ""
    echo ""

    container_id=$(docker -H  tcp://10.197.32.76:2375  ps | grep pdci_suite_production_jenkins_dood | awk -F " " ' { print $1 }')


    if [ -n ${container_id} ]; then



        echo ""
        echo ""
		    echo ""
        echo "*****************************"
        echo "Iniciado copia dos ultimos arquivos de configuracao em produção."
		    echo ""
		    echo ""
        echo "*****************************"
        echo "--- Copia do config.xml para override."
		    echo ""

        docker -H tcp://10.197.32.76:2375 cp  ${container_id}:/var/jenkins_home/config.xml config.xml.override

        echo ""
        echo ""
		    echo ""
		    echo "*****************************"
        echo "--- Copia do config.xml para override."
		    echo ""

        docker -H tcp://10.197.32.76:2375 cp  ${container_id}:/var/jenkins_home/config.xml config.xml.override
        echo "*****************************"
        echo " Removendo arquivo compactado do dump em tcp://10.197.32.76:2375"
        echo ""


    else
      echo "ERRO :: Nao foi possivel descobrir o container jenkins em tcp://10.197.32.76:2375 ."
      exit 1
    fi

}

copiar_files_dump


