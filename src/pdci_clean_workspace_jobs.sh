#!/bin/sh
set -e
for i in `ls  /var/jenkins_home/workspace/`
 do
  echo " realizando limpeza no workspace  /var/jenkins_home/workspace/$i "
  rm -rf /var/jenkins_home/workspace/$i/*
  rm -rf /var/jenkins_home/workspace/$i/.git/
  rm -rf /var/jenkins_home/workspace/$i/.git/*
  rm -rf /var/jenkins_home/workspace/$i/.env*
  rm -rf /var/jenkins_home/workspace/$i/.gitignore
  rm -rf /var/jenkins_home/workspace/$i/.gitattributes
  rm -rf /var/jenkins_home/workspace/$i/.gitlab-ci*
  rm -rf /var/jenkins_home/workspace/$i/.idea
  rm -rf /var/jenkins_home/workspace/$i/image_file_dump/.git/
  rm -rf /var/jenkins_home/workspace/$i/image_file_dump/dump/010-db_dev_cotec.sql.gz
done
