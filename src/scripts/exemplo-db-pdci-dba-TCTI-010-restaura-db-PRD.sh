#!/bin/bash

set -e
DIR=/backup/sincronizacao_tcti
DATE=`date "+D%y%m%d.H%H%M"`
#LOG_FILE=/usr/local/pgsql/rotinas/logs/sincronizacao_${DATE}.log
LOG_FILE=sincronizacao_${DATE}.log
DB_FILE_RESTORE=db_sisicmbio
DB_NAME_RESTORE=db_restaura_tcti_sisicmbio
DB_NAME_AFTER=db_tcti_sisicmbio
#NOME_ARQUIVO_DUMP_QZ=$(ls /backup/producao/dbprd01/pgsql/${DB_FILE_RESTORE}/${DB_FILE_RESTORE}* | head -1)
executa()
{
## criar diretorio para os indices / SSD conforme conf da infra
#sudo mkdir -p /index_ssd && chown -R postgres:postgres /index_ssd
#sudo mkdir -p /geo && chown -R postgres:postgres /geo

echo ""
echo "Montar pasta /backup"
echo ""

if mount | grep /backup > /dev/null; then
    echo "Pasta /backup já montada."
else
    echo "Inciciando comando de montagem."
    sudo mount -o rw,hard,intr,proto=tcp,port=2049,noatime -t nfs4 10.197.32.35:/backup /backup/
fi


echo ""
echo "Listando conteudo da pasta $DIR"
echo ""
sudo ls -lah $DIR


echo ""
echo "Removendo diretorio $DIR/${DB_FILE_RESTORE}*"
echo ""
sudo rm -f $DIR/${DB_FILE_RESTORE}*

sisicmbio=$(sudo /bin/ls -lhr /backup/producao/dbprd01/pgsql/${DB_FILE_RESTORE}/${DB_FILE_RESTORE}* | awk {'print $9'} | head -1)

#echo ""
#echo "Copiando arquivo $sisicmbio para  $DIR/"
#TEMPO_COPIA_INICIO=$(date "+DATE: %Y-%m-%d TIME: %H:%M:%S")
#echo "${TEMPO_COPIA_INICIO}"
#echo ""
#sudo cp -v $sisicmbio $DIR
#TEMPO_COPIA_TERMINO=$(date "+DATE: %Y-%m-%d TIME: %H:%M:%S")
#echo "${TEMPO_COPIA_TERMINO}"
#echo ""
#echo "Finalizado copia."
#echo ""
#echo ""
#echo "Descompactando "
#echo ""
#sudo gunzip -v ${DIR}/${DB_FILE_RESTORE}*
#
#echo ""
#echo "Renomeando para  ${DIR}/${DB_FILE_RESTORE}"
#echo ""
#sudo mv ${DIR}/${DB_FILE_RESTORE}* ${DIR}/${DB_FILE_RESTORE}.dump
#
#ls -la ${DIR}/

#NOME_ARQUIVO_DUMP_QZ=$(ls ${DIR}/${DB_FILE_RESTORE}* | head -1)
#echo "NOME_ARQUIVO_DUMP_QZ => ${NOME_ARQUIVO_DUMP_QZ}"



sudo /usr/local/pgsql/bin/psql -U postgres -d postgres -c "select pg_terminate_backend(pid) from pg_stat_activity where datname='${DB_NAME_RESTORE}';"
sudo /usr/local/pgsql/bin/psql -U postgres -d postgres -c "drop database IF EXISTS ${DB_NAME_RESTORE};"
sudo /usr/local/pgsql/bin/psql -U postgres -d postgres -c "create database ${DB_NAME_RESTORE};"
#sudo /usr/local/pgsql/bin/psql -U postgres -d ${DB_NAME_RESTORE} -c "DROP TABLESPACE  IF EXISTS tbs_index_ssd;";
#sudo /usr/local/pgsql/bin/psql -U postgres -d ${DB_NAME_RESTORE} -c "CREATE TABLESPACE tbs_index_ssd LOCATION '/index_ssd';";
#sudo /usr/local/pgsql/bin/psql -U postgres -d ${DB_NAME_RESTORE} -c "DROP TABLESPACE  IF EXISTS tbs_geo;";
#sudo /usr/local/pgsql/bin/psql -U postgres -d ${DB_NAME_RESTORE} -c "CREATE TABLESPACE tbs_geo LOCATION '/geo';";
echo ""
echo "Restaurando dump."
echo ""
TEMPO_INICIO_DA_RESTAURACAO=$(date "+DATE: %Y-%m-%d TIME: %H:%M:%S")
echo "TEMPO INICIO DA RESTAURACAO: ${TEMPO_INICIO_DA_RESTAURACAO}"
echo ""
#sudo /usr/local/pgsql/bin/psql -U postgres -d ${DB_NAME_RESTORE} -f $DIR/${DB_FILE_RESTORE}.dump
sudo gunzip -c "${sisicmbio}" | /usr/local/pgsql/bin/psql -U postgres -d ${DB_NAME_RESTORE} ;
echo ""
TEMPO_TERMINO_DA_RESTAURACAO=$(date "+DATE: %Y-%m-%d TIME: %H:%M:%S")
echo "*************************************************************************"
echo ""
echo "Inicio da copia do arquivo de dump:  ${TEMPO_COPIA_INICIO}."
echo "Termino da copia do arquivo de dump:  ${TEMPO_COPIA_TERMINO}."
echo "TEMPO INICIO DA RESTAURACAO: ${TEMPO_INICIO_DA_RESTAURACAO}"
echo "TEMPO TERMINO DA RESTAURACAO: ${TEMPO_TERMINO_DA_RESTAURACAO}"
echo ""
echo "*************************************************************************"
echo ""
echo ""
echo "Grant's"
echo ""
sudo /usr/local/pgsql/bin/psql -U postgres -d postgres -c "GRANT ALL ON DATABASE ${DB_NAME_RESTORE} TO postgres;"
sudo /usr/local/pgsql/bin/psql -U postgres -d postgres -c "GRANT CONNECT, CREATE ON DATABASE ${DB_NAME_RESTORE} TO usr_jenkins;"

if mount | grep /backup > /dev/null; then
#    sudo ls -la ${DIR}
    echo "Desmontando pasta /backup."
#    sudo rm -f ${DIR}/${DB_FILE_RESTORE}*
#    sudo ls -la ${DIR}
    sudo umount /backup
fi

#

#echo "Realizada a atualicacao com sucesso" >> ${LOG_FILE}

sudo /usr/local/pgsql/bin/psql -U postgres -d postgres -c "select pg_terminate_backend(pid) from pg_stat_activity where datname='${DB_NAME_RESTORE}';"
sudo /usr/local/pgsql/bin/psql -U postgres -d postgres -c "select pg_terminate_backend(pid) from pg_stat_activity where datname='${DB_NAME_AFTER}';"
sudo /usr/local/pgsql/bin/psql -U postgres -d postgres -c "drop database IF EXISTS ${DB_NAME_AFTER};"
sudo /usr/local/pgsql/bin/psql -U postgres -d postgres -c "ALTER DATABASE ${DB_NAME_RESTORE} RENAME TO ${DB_NAME_AFTER};"

}

executa



