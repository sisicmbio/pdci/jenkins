#!/bin/bash

set -e
###################################################################################
#JENKINS_URL=${JENKINS_URL:-http://${pdci_suite_jenkins_domain}:8080}
#JENKINS_URL=http://127.0.0.1:8080
#JENKINS_URL=http://127.0.0.1:8080
JENKINS_URL=http://10.197.32.76:51082
echo "JENKINS_URL => ${JENKINS_URL}"

if [ "${PDCI_REDMINE_URL}" = "" ]; then
  echo ""
  echo ""
  echo ""
  echo "================ ERROR================="
  echo ""
  echo "Favor informar o parametro PDCI_REDMINE_URL."
  echo ""
  echo ""
  exit 1

fi

if [ "${PDCI_REDMINE_URL}" == "https://sistemascotec.icmbio.gov.br" ]; then

  __PDCI_REDMINE_API_KEY=${__PDCI_REDMINE_SISTEMASCOTEC_API_KEY}

else

  __PDCI_REDMINE_API_KEY=${__PDCI_REDMINE_GESTAO_API_KEY}

fi

if [ "${PDCI_REDMINE_URL_INFRA}" == "https://sistemascotec.icmbio.gov.br" ]; then

  __PDCI_REDMINE_INFRA_SELECIONADO_API_KEY=${__PDCI_REDMINE_SISTEMASCOTEC_API_KEY}

else

  __PDCI_REDMINE_INFRA_SELECIONADO_API_KEY=${__PDCI_REDMINE_GESTAO_API_KEY}

fi

if [ "${__PDCI_REDMINE_API_KEY}" = "" ]; then
  echo ""
  echo ""
  echo ""
  echo "================ ERROR================="
  echo ""
  echo "Favor informar API KEY do usuário no Redmine ${PDCI_REDMINE_URL}, em Configure (Configuração) deste Job."
  echo ""
  echo ""
  exit 1

fi

echo "Criando jobs previstos no PDCI no jenkins para o projeto: ${PDCI_URL_GITLAB_PROJETO}"
echo ""
echo ""
git clone https://gitlab+deploy-token-50060:d7RApxRvoVjaYrRcYzkR@gitlab.com/pdci/pdci-jobs-jenkins.git
cd pdci-jobs-jenkins
PDCI_GIT_SHORT_SHA=$(git rev-parse --short HEAD)

echo "Download do jenkins cli em : ${JENKINS_URL}/jnlpJars/jenkins-cli.jar"
echo ${JENKINS_URL}
wget --no-check-certificate ${JENKINS_URL}/jnlpJars/jenkins-cli.jar
rm -rf jenkins-cli.jar.*

atualizar_job() {

  echo "s|[\$]{PDCI_URL_GITLAB_PROJETO}|${PDCI_URL_GITLAB_PROJETO}|g"
  echo "s|[\$]{PDCI_REGISTRY}|${PDCI_REGISTRY}|g" $i
  echo "s|[\$]{PDCI_DOCKER_HOST_TCTI}|${pdci_docker_host_tcti}|g"
  echo "s|[\$]{PDCI_REGISTRY_IMAGE_FILE_DUMP}|${__PDCI_REGISTRY_IMAGE_FILE_DUMP}|g"
  echo "s|[\$]{PDCI_REGISTRY_IMAGE_DB}|${__PDCI_REGISTRY_IMAGE_DB}|g"
  echo "s|[\$]{PDCI_DOCKER_HOST_DEV}|${pdci_docker_host_dev}|g"
  echo "s|[\$]{PDCI_DOCKER_HOST_TCTI}|${pdci_docker_host_tcti}|g"
  echo "s|[\$]{PDCI_DOCKER_HOST_HMG}|${pdci_docker_host_hmg}|g"
  echo "s|[\$]{PDCI_DOCKER_HOST_PRD}|${pdci_docker_host_prd}|g"

  PDCI_URL_IMAGE_FILE_DUMP=$(echo "${PDCI_URL_GITLAB_PROJETO}/image_file_dump-dev" | sed -e "s|https:\/\/gitlab.com|${PDCI_REGISTRY}|g" | sed -e "s|\.git\/|\/|g")

  name_group=$(echo ${PDCI_URL_GITLAB_PROJETO} | awk -F'/' '{printf $4}')
  #nome_projeto=$(echo ${PDCI_URL_GITLAB_PROJETO} | awk -F'/' '{printf $5}'  | sed -e "s/.git//g" )
  #nome_projeto_mainapp=$(echo ${PDCI_URL_GITLAB_PROJETO} | awk -F'/' '{printf $5}' | sed -e "s/.git//g" | sed -e "s/app_//g" )

  v_local_name_project=$(grep -o "[\/]" <<<"$PDCI_URL_GITLAB_PROJETO" | wc -l)
  #echo "v_local_name_project => $v_local_name_project"

  IFS='/' read -ra PARAMETROS <<<"$PDCI_URL_GITLAB_PROJETO"
  #echo "${PARAMETROS[${v_local_name_project}]}" | sed -e "s/.git//g" | sed -e "s/app_//g"
  nome_projeto_mainapp=$(echo "${PARAMETROS[${v_local_name_project}]}" | sed -e "s/.git//g" | sed -e "s/app_//g")
  #echo "__pdci_nome_projeto_mainapp => ${__pdci_nome_projeto_mainapp}"
  nome_projeto=$(echo "${PARAMETROS[${v_local_name_project}]}" | sed -e "s/.git//g")

  ## Lista de JOBS Para DEV TCTI e HMG para a equipe de desenvolvedores

  ## declare an array variable
  if [[ "${PDCI_LIST_JOBS_DEVELOPER}" = "" ]]; then

    declare -a arr_all_jobs=("exemplo-pdci-{app_nome}-004-customizar-bd-image_file_dump.xml"
      "exemplo-pdci-{app_nome}-005-registry-image_file_dump-dev.xml"
      "exemplo-pdci-{app_nome}-006-registry-postgis-dev.xml"
      "exemplo-pdci-{app_nome}-020-upd-sql-em-TCTI-db.xml"
      "exemplo-pdci-{app_nome}-021-deploy-em-TCTI-php.xml"
      "exemplo-pdci-{app_nome}-022-deploy-k8s-TCTI-app.xml"
      "exemplo-pdci-{app_nome}-022-deploy-em-TCTI-app-log.xml"
      "exemplo-pdci-{app_nome}-023-em-TCTI-nginx-proxy-log.xml"
      "exemplo-pdci-{app_nome}-030-upd-sql-em-HMG-db.xml"
      "exemplo-pdci-{app_nome}-031-deploy-em-HMG-php.xml"
      "exemplo-pdci-{app_nome}-032-deploy-k8s-HMG-app.xml"
      "exemplo-pdci-{app_nome}-033-build-dump-HMG-db.xml"
      "exemplo-pdci-{app_nome}-033-em-HMG-nginx-proxy-log.xml"
      "exemplo-pdci-{app_nome}-034-exec-em-HMG.xml"
      "exemplo-pdci-{app_nome}-040-upd-sql-em-PRD-db.xml"
      "exemplo-pdci-{app_nome}-041-deploy-em-PRD-php.xml"
      "exemplo-pdci-{app_nome}-042-deploy-k8s-PRD-app.xml"
      "exemplo-pdci-{app_nome}-022-deploy-em-TCTI-app-log.xml"
      "exemplo-pdci-{app_nome}-032-deploy-em-HMG-app-log.xml"
      "exemplo-pdci-{app_nome}-042-deploy-em-PRD-app-log.xml"
      "exemplo-pdci-{app_nome}-043-em-PRD-nginx-proxy-log.xml"
      "exemplo-pdci-{app_nome}-050-abertura-chamado-deploy-db-PRD.xml"
      "exemplo-pdci-{app_nome}-051-abertura-chamado-deploy-app-PRD.xml"
      "exemplo-pdci-{app_nome}-061-deploy-em-TRN-app-log.xml"
      "exemplo-pdci-{app_nome}-061-deploy-em-TRN-app.xml"
      "exemplo-pdci-{app_nome}-060-upd-sql-em-TRN-db.xml"
      "exemplo-pdci-{app_nome}-063-em-TRN-nginx-proxy-log.xml"
      "exemplo-pdci-{app_nome}-026-deploy-k8s-TCTI-app.xml"
      "exemplo-pdci-{app_nome}-036-deploy-k8s-HMG-app.xml"
      "exemplo-pdci-{app_nome}-046-deploy-k8s-PRD-app.xml"
      "exemplo-pdci-{app_nome}-066-deploy-k8s-TRN-app.xml")


  else

    declare -a arr_all_jobs=(${PDCI_LIST_JOBS_DEVELOPER})

  fi \
    ;

  ## now loop through the above array
  for i in "${arr_all_jobs[@]}"; do #for i in `ls exemplo*{app_nome}*.xml`
    #for i in `ls exemplo-pdci-010-build-php-app_infocon*`

    __PDCI_REGISTRY_IMAGE_APP=${PDCI_REGISTRY}/${name_group}/${nome_projeto}
    __PDCI_REGISTRY_IMAGE_FILE_DUMP=${PDCI_REGISTRY}/${name_group}/${nome_projeto}/image_file_dump-dev
    __PDCI_URL_IMAGE_FILE_DUMP_HMG=${PDCI_REGISTRY}/${name_group}/${nome_projeto}/image_file_dump-hmg
    __PDCI_REGISTRY_IMAGE_DB=${PDCI_REGISTRY}/${name_group}/${nome_projeto}/postgis-dev
    __PDCI_DB_SCHEMA_APP=$(echo ${nome_projeto} | sed -e "s|app_||g")
    echo "Job de exemplo a ser usado: $i"
    echo "Substituindo variaveis"
    # cat $i
    echo ""
    echo ""
    sed -i "s|[\$]{PDCI_URL_GITLAB_PROJETO}|${PDCI_URL_GITLAB_PROJETO}|g" $i
    sed -i "s|[\$]{PDCI_DB_SCHEMA_APP}|${__PDCI_DB_SCHEMA_APP}|g" $i
    sed -i "s|[\$]{PDCI_URL_IMAGE_FILE_DUMP}|${PDCI_URL_IMAGE_FILE_DUMP}|g" $i
    sed -i "s|[\$]{PDCI_URL_IMAGE_FILE_DUMP_HMG}|${__PDCI_URL_IMAGE_FILE_DUMP_HMG}|g" $i
    sed -i "s|[\$]{PDCI_REGISTRY}|${PDCI_REGISTRY}|g" $i
    sed -i "s|[\$]{PDCI_DOCKER_HOST_TCTI}|${pdci_docker_host_tcti}|g" $i
    sed -i "s|[\$]{PDCI_REGISTRY_IMAGE_FILE_DUMP}|${__PDCI_REGISTRY_IMAGE_FILE_DUMP}|g" $i
    sed -i "s|[\$]{PDCI_REGISTRY_IMAGE_DB}|${__PDCI_REGISTRY_IMAGE_DB}|g" $i
    sed -i "s|[\$]{PDCI_DOCKER_HOST_DEV}|${pdci_docker_host_dev}|g" $i
    sed -i "s|[\$]{PDCI_DOCKER_HOST_TCTI}|${pdci_docker_host_tcti}|g" $i
    sed -i "s|[\$]{PDCI_DOCKER_HOST_HMG}|${pdci_docker_host_hmg}|g" $i
    sed -i "s|[\$]{PDCI_DOCKER_HOST_PRD}|${pdci_docker_host_prd}|g" $i
    sed -i "s|{projeto}|${nome_projeto_mainapp}|g" $i
    sed -i "s|__app_nome__|${nome_projeto}|g" $i
    #    sed -i "s|[\$]{name_group}|${name_group}|g" $i
    sed -i "s|[\$]{nome_projeto}|${nome_projeto}|g" $i
    sed -i "s|[\$]{nome_projeto_mainapp}|${nome_projeto_mainapp}|g" $i
    sed -i "s|[\$]{pdci_docker_host_prd}|${pdci_docker_host_prd}|g" $i
    sed -i "s|[\$]{pdci_docker_host_dev}|${pdci_docker_host_dev}|g" $i
    sed -i "s|[\$]{pdci_docker_host_tcti}|${pdci_docker_host_tcti}|g" $i
    sed -i "s|[\$]{pdci_docker_host_hmg}|${pdci_docker_host_hmg}|g" $i
    echo ""
    echo ""
    echo "Trocar o nome da referencia a outros jobs"
    sed -i "s|{db_prd_name}|${PDCI_BD_NAME_PRD}|g" $i
    sed -i "s|exemplo-db-pdci-|pdci-|g" $i
    echo ""
    echo ""
    nome_job_novo=$(echo $i | sed -e "s/exemplo-pdci-{app_nome}/pdci-${nome_projeto}/g" | sed -e "s/.xml//g")

    	case ${nome_job_novo} in
            *"deploy-k8s-TCTI-app"*)
                sed -i "s|__usr__projeto__|db_tcti_usr_${nome_projeto_mainapp}|g" $i
                curl  -X POST 'http://69974349168:11ddb9aa0d13e417b1296e52c6958677b4@127.0.0.1:8080/credentials/store/system/domain/_/createCredentials' --data-urlencode 'json={
                    "": "0",
                    "credentials": {
                      "scope": "GLOBAL",
                      "id": "db_tcti_usr_'"${nome_projeto_mainapp}"'",
                      "username": "usr_'"${nome_projeto_mainapp}"'",
                      "password": "usr_'"${nome_projeto_mainapp}"'",
                      "description": "Credenciais do DB do projeto '"${nome_projeto}"' em TCTI.",
                      "$class": "com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl"
                    }
                  }'
                ;;
            *"deploy-k8s-HMG-app"*)
                sed -i "s|__usr__projeto__|db_hmg_usr_${nome_projeto_mainapp}|g" $i
                curl  -X POST 'http://69974349168:11ddb9aa0d13e417b1296e52c6958677b4@127.0.0.1:8080/credentials/store/system/domain/_/createCredentials' --data-urlencode 'json={
                    "": "0",
                    "credentials": {
                      "scope": "GLOBAL",
                      "id": "db_hmg_usr_'"${nome_projeto_mainapp}"'",
                      "username": "usr_'"${nome_projeto_mainapp}"'",
                      "password": "usr_'"${nome_projeto_mainapp}"'",
                      "description": "Credenciais do DB do projeto '"${nome_projeto}"' em Homologação.",
                      "$class": "com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl"
                    }
                  }'
                ;;
            *"deploy-k8s-PRD-app"*)
                sed -i "s|__usr__projeto__|db_prd_usr_${nome_projeto_mainapp}|g" $i
                curl  -X POST 'http://69974349168:11ddb9aa0d13e417b1296e52c6958677b4@127.0.0.1:8080/credentials/store/system/domain/_/createCredentials' --data-urlencode 'json={
                    "": "0",
                    "credentials": {
                      "scope": "GLOBAL",
                      "id": "db_prd_usr_'"${nome_projeto_mainapp}"'",
                      "username": "usr_'"${nome_projeto_mainapp}"'",
                      "password": "usr_'"${nome_projeto_mainapp}"'",
                      "description": "Credenciais do DB do projeto '"${nome_projeto}"' em Produção.",
                      "$class": "com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl"
                    }
                  }'
                ;;
            *"deploy-k8s-TRN-app"*)
                sed -i "s|__usr__projeto__|db_trn_usr_${nome_projeto_mainapp}|g" $i
                curl  -X POST 'http://69974349168:11ddb9aa0d13e417b1296e52c6958677b4@127.0.0.1:8080/credentials/store/system/domain/_/createCredentials' --data-urlencode 'json={
                    "": "0",
                    "credentials": {
                      "scope": "GLOBAL",
                      "id": "db_trn_usr_'"${nome_projeto_mainapp}"'",
                      "username": "usr_'"${nome_projeto_mainapp}"'",
                      "password": "usr_'"${nome_projeto_mainapp}"'",
                      "description": "Credenciais do DB do projeto '"${nome_projeto}"' em Treinamento.",
                      "$class": "com.cloudbees.plugins.credentials.impl.UsernamePasswordCredentialsImpl"
                    }
                  }'
                ;;
             *)

                echo ""
                ;;
        esac

    echo "Job a ser criado: ${nome_job_novo}"
    echo ""
    echo "Criando se não existir: ${nome_job_novo}"
    java -jar jenkins-cli.jar -s ${JENKINS_URL} create-job ${nome_job_novo} <${i} || true
    echo "Atualizando se existir: ${nome_job_novo}"
    java -jar jenkins-cli.jar -s ${JENKINS_URL} update-job ${nome_job_novo} <${i} || true
    echo ""
    echo ""

    cat ${i} >${nome_job_novo}.xml

  done

  echo "Criando jobs de BD de produção previstos no PDCI no jenkins: ${PDCI_URL_GITLAB_PROJETO}"
  echo ""
  echo ""
  ## declare an array variable
  declare -a arr=("exemplo-db-pdci-008-cp_last_file_dump-prd.xml"
    "exemplo-db-pdci-002-{db_prd_name}-db-restaura-image_file_dump-prd.xml"
    "exemplo-db-pdci-004-{db_prd_name}-customizar-bd-projetos.xml"
    "exemplo-pdci-030-export-jobs-exemplos-suite.xml")

  ## declare an array variable
  #declare -a arr=("exemplo-db-pdci-000-{db_prd_name}-db-build-image_file_dump-prd.xml"
  #"exemplo-db-pdci-001-{db_prd_name}-db-push-registry-image_file_dump-prd.xml"
  #"exemplo-db-pdci-002-{db_prd_name}-db-restaura-image_file_dump-prd.xml"
  #"exemplo-db-pdci-003-{db_prd_name}-db-push-registry-postgis-prd.xml"
  #"exemplo-db-pdci-004-{db_prd_name}-customizar-bd-projetos.xml")

  ## now loop through the above array
  for i in "${arr[@]}"; do #for i in `ls exemplo-db-pdci-*{db_prd_name}*.xml`

    if [[ -f "$i" ]]; then
      echo "Job BD PRD de explo a ser usado: $i"
      echo "Substituindo variaveis"
      #   cat $i
      echo ""
      echo ""
      sed -i "s|[\$]{PDCI_URL_GITLAB_PROJETO}|${PDCI_URL_GITLAB_PROJETO}|g" $i
      sed -i "s|[\$]{PDCI_URL_IMAGE_FILE_DUMP}|${PDCI_URL_IMAGE_FILE_DUMP}|g" $i
      sed -i "s|[\$]{PDCI_REGISTRY}|${PDCI_REGISTRY}|g" $i
      sed -i "s|[\$]{PDCI_DOCKER_HOST_DEV}|${pdci_docker_host_dev}|g" $i
      sed -i "s|[\$]{PDCI_DOCKER_HOST_TCTI}|${pdci_docker_host_tcti}|g" $i
      sed -i "s|[\$]{PDCI_DOCKER_HOST_HMG}|${pdci_docker_host_hmg}|g" $i
      sed -i "s|[\$]{PDCI_DOCKER_HOST_PRD}|${pdci_docker_host_prd}|g" $i
      sed -i "s|[\$]{PDCI_BD_NAME_PRD}|${PDCI_BD_NAME_PRD}|g" $i
      sed -i "s|__PDCI_BD_NAME_PRD__|${PDCI_BD_NAME_PRD}|g" $i
      sed -i "s|[\$]{PDCI_IMAGE_NAME_POSTGIS_PRD}|${PDCI_IMAGE_NAME_POSTGIS_PRD}|g" $i
      sed -i "s|[\$]{name_group}|${name_group}|g" $i
      sed -i "s|[\$]{nome_projeto}|${nome_projeto}|g" $i
      sed -i "s|[\$]{nome_projeto_mainapp}|${nome_projeto_mainapp}|g" $i
      sed -i "s|[\$]{pdci_docker_host_prd}|${pdci_docker_host_prd}|g" $i
      sed -i "s|[\$]{pdci_docker_host_dev}|${pdci_docker_host_dev}|g" $i
      sed -i "s|[\$]{pdci_docker_host_tcti}|${pdci_docker_host_tcti}|g" $i
      sed -i "s|[\$]{pdci_docker_host_trn}|${pdci_docker_host_trn}|g" $i
      sed -i "s|[\$]{pdci_docker_host_hmg}|${pdci_docker_host_hmg}|g" $i
      sed -i "s|exemplo-db-pdci-|pdci-|g" $i

      echo ""
      echo ""
      #arquivo_xml=$(cat $i | sed -e "s/${PDCI_URL_GITLAB_PROJETO}/${PDCI_URL_GITLAB_PROJETO}/g")
      #cat arquivo_xml

      nome_job_db_novo=$(echo $i | sed -e "s|exemplo-db-pdci-|pdci-|g" | sed -e "s/{db_prd_name}/${PDCI_BD_NAME_PRD}/g" | sed -e "s/.xml//g")

      echo ""
      echo ""
      echo "Trocar o nome da referencia a outros jobs"
      sed -i "s|{db_prd_name}|${PDCI_BD_NAME_PRD}|g" $i
      sed -i "s|exemplo-db-pdci-|pdci-|g" $i

      echo "Job de DB ser criado: ${nome_job_db_novo}"
      #retorno=$(java -jar jenkins-cli.jar -s  ${JENKINS_URL} create-job ${nome_job_novo} < ${i})
      #java -jar jenkins-cli.jar -s  ${JENKINS_URL}  get-job  ${nome_job_novo} > existe
      #cat existe
      echo ""
      echo "Criando se não existir: ${nome_job_db_novo}"
      java -jar jenkins-cli.jar -s ${JENKINS_URL} create-job ${nome_job_db_novo} <${i} || true
      echo "Atualizando se existir: ${nome_job_db_novo}"
      java -jar jenkins-cli.jar -s ${JENKINS_URL} update-job ${nome_job_db_novo} <${i} || true
      echo ""
      echo ""

    fi

  done

  #pwd

  echo ""
  echo "nome_projeto => ${nome_projeto}"
  echo ""
  cp views/exemplo_nome_projeto.xml views/exemplo_${nome_projeto}.xml

  sed -i "s|exemplo_nome_projeto|${nome_projeto}|g" views/exemplo_${nome_projeto}.xml

  cat views/exemplo_${nome_projeto}.xml
  echo ""
  echo "Criando View para o projeto previstos no PDCI para o jenkins: ${PDCI_URL_GITLAB_PROJETO}"
  echo ""
  echo "Criando View caso não exista: ${nome_projeto}"
  java -jar jenkins-cli.jar -s ${JENKINS_URL} create-view ${nome_projeto} <views/exemplo_${nome_projeto}.xml || true
  echo "Atualizando View caso exista: ${nome_projeto}"
  java -jar jenkins-cli.jar -s ${JENKINS_URL} update-view ${nome_projeto} <views/exemplo_${nome_projeto}.xml || true
  echo ""
  echo ""

  #ATRIBUIR PERMISSAO A LISTA DE USUARIOS CONFORME PERFIL ATRIBUIDO NO REDMINE
  #wget https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
  #chmod +x jq-linux64
  #mv jq-linux64 jq
  #ls -la

  #descobrir o id do projeto no redmine.
  __PDCI_REDMINE_PERFIL_INFRA_BD=${PDCI_REDMINE_PERFIL_INFRA_BD}
  __PDCI_REDMINE_PERFIL_INFRA_DEPLOY=${PDCI_REDMINE_PERFIL_INFRA_DEPLOY}
  __PDCI_REDMINE_PERFIL_DESENVOLVEDOR=${PDCI_REDMINE_PERFIL_DESENVOLVEDOR}
  __PDCI_REDMINE_PERFIL_SCRUM=${PDCI_REDMINE_PERFIL_SCRUM}
  __PDCI_REDMINE_URL=${PDCI_REDMINE_URL}
  __PDCI_REDMINE_FIND_GITLAB_PROJECT=${PDCI_REDMINE_FIND_GITLAB_PROJECT}
  __PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB=${PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}
  #__PDCI_REDMINE_API_KEY=${PDCI_REDMINE_API_KEY}

  #if [ "${PDCI_REDMINE_URL}" == "https://sistemascotec.icmbio.gov.br" ]; then
  #
  ##__PDCI_REDMINE_API_KEY=${__PDCI_REDMINE_SISTEMASCOTEC_API_KEY}
  #
  #else
  #
  ##__PDCI_REDMINE_API_KEY=${__PDCI_REDMINE_SISTEMASCOTEC_API_KEY}
  #echo "jq '.projects[] | select( .custom_fields[].name  == \"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" and  .custom_fields[].value == \"${PDCI_URL_GITLAB_PROJETO}\"   )? | . '   projects.json  | jq --raw-output '.id' " > cmd_project
  #
  #fi

  #__PDCI_REDMINE_PERFIL_INFRA_BD=PDCI-INFRA-BD
  #__PDCI_REDMINE_PERFIL_INFRA_DEPLOY=PDCI-INFRA-DEPLOY
  #__PDCI_REDMINE_PERFIL_DESENVOLVEDOR=PDCI-DESENVOLVEDOR
  #__PDCI_REDMINE_URL=https://sistemascotec.icmbio.gov.br
  #__PDCI_REDMINE_FIND_GITLAB_PROJECT=$(echo ${PDCI_URL_GITLAB_PROJETO} | awk -F'/' '{printf $5}'  | sed -e "s/.git//g" )
  #__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB='PDCI Gitlab Url Project'
  #__PDCI_REDMINE_API_KEY=dd117b7ba5fc9f84a47a40ba551bb52b544952ff

  #Adicionando PERFIL de BIULD de BD EM HMG APENAS PARA O

  echo "curl -k -v -H  --insecure \"Content-Type: application/json\" -X GET -H \"X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}\" ${__PDCI_REDMINE_URL}/projects.json?limit=200 > projects.json" >comando-project

  curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/projects.json?limit=200 >projects.json

#  __PDCI_PROJET0_URL_REDMINE_DEV="${PDCI_REDMINE_URL}/projects/$(jq '.projects[].identifier' projects.json)/"
#
#  echo "__PDCI_PROJET0_URL_REDMINE_DEV =>${__PDCI_PROJET0_URL_REDMINE_DEV}"
#  exit 1

  if [ "${PDCI_REDMINE_URL}" == "https://sistemascotec.icmbio.gov.br" ]; then

    echo "jq '.projects[] | select( ( .custom_fields[].value  | contains(\"${PDCI_URL_GITLAB_PROJETO}\") ) and  .custom_fields[].name ==\"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" )? '  projects.json | jq --raw-output '.id' " >cmd_project

  else

    echo "jq '.projects[] | select( .custom_fields[].name  == \"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" and  .custom_fields[].value == \"${PDCI_URL_GITLAB_PROJETO}\"   )? | . '   projects.json  | jq --raw-output '.id' " >cmd_project

  fi

  chmod +x cmd_project
  ./cmd_project
  project_id=$(./cmd_project)

  if [ "${project_id}" == "" ]; then

    echo ""
    echo ""
    echo "***********************************   ERRO   ***********************************"
    echo ""
    echo "NAO foi encontrado nenhum projeto no redmine (${__PDCI_REDMINE_URL})"
    echo "    com o campo \"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" contendo a palavra \"${PDCI_URL_GITLAB_PROJETO}\" "
    echo "       nas configuracoes dos projetos."
    echo ""
    echo "================================================================================"
    echo ""
    exit 1
  fi

  #==================================================Descobrir o project_infra_id no redmine ${PDCI_REDMINE_URL_INFRA} =========================================================================

  echo "curl -k -v -H  --insecure \"Content-Type: application/json\" -X GET -H \"X-Redmine-API-Key: ${__PDCI_REDMINE_INFRA_SELECIONADO_API_KEY}\" ${PDCI_REDMINE_URL_INFRA}/projects.json?limit=200 > infra_projects.json" >cmd_create_infra_projects.json
  curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_INFRA_SELECIONADO_API_KEY}" ${PDCI_REDMINE_URL_INFRA}/projects.json?limit=200 >infra_projects.json

  if [ "${PDCI_REDMINE_URL}" == "https://sistemascotec.icmbio.gov.br" ]; then

    echo "jq '.projects[] | select( ( .custom_fields[].value  | contains(\"${PDCI_URL_GITLAB_PROJETO}\") ) and  .custom_fields[].name ==\"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" )? '  infra_projects.json | jq --raw-output '.id' " >cmd_infra_project

  else

    echo "jq '.projects[] | select( .custom_fields[].name  == \"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" and  .custom_fields[].value == \"${PDCI_URL_GITLAB_PROJETO}\"   )? | . '   infra_projects.json  | jq --raw-output '.id' " >cmd_infra_project

  fi

  chmod +x cmd_infra_project
  ./cmd_infra_project
  infra_project_id=$(./cmd_infra_project)

  if [ "${infra_project_id}" == "" ]; then

    echo ""
    echo ""
    echo "***********************************   ERRO   ***********************************"
    echo ""
    echo "NAO foi encontrado nenhum projeto no redmine (${PDCI_REDMINE_URL_INFRA})"
    echo "    com o campo \"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" contendo a palavra \"${PDCI_URL_GITLAB_PROJETO}\" "
    echo "       nas configuracoes dos projetos."
    echo ""
    echo "================================================================================"
    echo ""
    exit 1
  fi

  #====================================================PERMISSAO DE LEITURA EM TODOS OS JOBS DO PROJETO PARA EQUIPE DE DESENVOLVIMENTO E INFRA ===============================

  curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/projects/${project_id}/memberships.json?limit=200 >memberships.json

  #===========================================ADICIONADO A EQUIPE DE INFRA
  curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_INFRA_SELECIONADO_API_KEY}" ${PDCI_REDMINE_URL_INFRA}/projects/${infra_project_id}/memberships.json?limit=200 >>memberships.json

  #curl -k -v -H  --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: dd117b7ba5fc9f84a47a40ba551bb52b544952ff" https://sistemascotec.icmbio.gov.br//projects/${project_id}/memberships.json > memberships.json

  #jq --raw-output '.memberships[] | select( (.roles[].name|index("PDCI-INFRA-DEPLOY")) or (.roles[].name|index("PDCI-INFRA-BD")) or (.roles[].name|index("PDCI-DESENVOLVEDOR"))  ) ' memberships.json

  if [ "${PDCI_REDMINE_URL}" == "https://sistemascotec.icmbio.gov.br" ]; then

    echo "jq '.memberships[] | select( (.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_INFRA_BD}\")) or (.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_INFRA_DEPLOY}\")) or (.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_DESENVOLVEDOR}\"))  ) ' memberships.json  |  jq --raw-output '.user.id'" >cmd_ids_EQUIPE

  else

    echo "jq '.memberships[] | select( (.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_INFRA_BD}\")) or (.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_INFRA_DEPLOY}\")) or (.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_DESENVOLVEDOR}\"))  ) ' memberships.json  |  jq --raw-output '.user.id'" >cmd_ids_EQUIPE

  fi
  #echo "jq --raw-output '.memberships[] | select(.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_INFRA_DEPLOY}\"))' memberships.json |  jq '.user.id'" > cmd_ids_EQUIPE

  chmod +x cmd_ids_EQUIPE
  ./cmd_ids_EQUIPE

  ids_EQUIPE=$(./cmd_ids_EQUIPE)

  echo "ids_EQUIPE => ${ids_EQUIPE}"

  if [ "${ids_EQUIPE}" != "" ]; then

    for user_id in ${ids_EQUIPE}; do
      echo "==============================="
      echo ""
      echo "Buscando dados para user_id => ${user_id}"
      echo "${__PDCI_REDMINE_URL}/users/${user_id}.json"
      curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/users/${user_id}.json | jq --raw-output .user.login >${user_id}.user_username.txt
      curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/users/${user_id}.json | jq --raw-output .user.firstname >${user_id}.firstname.txt
      #user_username=$(curl -k -v -H  --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" https://sistemascotec.icmbio.gov.br/users/${user_id}.json | jq --raw-output .user.login)
      user_username=$(cat ${user_id}.user_username.txt)
      user_firstname=$(cat ${user_id}.firstname.txt)
      echo "-------------------------------------------------------"

      echo "user_username => ${user_username}"

      #   exit 1

      for i in $(ls pdci-${nome_projeto}-*.xml); do
        if [[ -f "$i" ]]; then
          echo "EQUIPE :: Atribuindo permissao de leitura ao usuario: ${user_username} ($user_firstname) no job $i ."
          echo ""
          echo ""
          sed -i "s|<inheritanceStrategy class=\"org.jenkinsci.plugins.matrixauth.inheritance.InheritParentStrategy\"\/>|<inheritanceStrategy class=\"org.jenkinsci.plugins.matrixauth.inheritance.InheritParentStrategy\"\/><permission>hudson.model.Item.Discover:${user_username}</permission><permission>hudson.model.Item.Read:${user_username}</permission><permission>hudson.model.Item.Workspace:${user_username}</permission>|g" $i
        fi
      done

    done
  else
    echo ""
    echo "Projeto sem atrubuição no redmine de ${__PDCI_REDMINE_PERFIL_INFRA_BD} ou ${__PDCI_REDMINE_PERFIL_INFRA_DEPLOY} ou ${__PDCI_REDMINE_PERFIL_DESENVOLVEDOR=PDCI-DESENVOLVEDOR}"
    echo ""
    exit 1
  fi

  #====================================================PERMISSAO DA EQUIPE DE DESENVOLVIMENTO NOS JOBS DE DEPLOY e ATUALIZACAO DE SQL NOS AMBIENTES TCTI E HMG ===============================
  echo ""
  echo ""
  echo "======================PERMISSAO DA EQUIPE DE DESENVOLVIMENTO NOS JOBS DE DEPLOY e ATUALIZACAO DE SQL NOS AMBIENTES TCTI E HMG ============"
  echo ""
  echo ""
  curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/projects/${project_id}/memberships.json?limit=200 >memberships.json

  if [ "${PDCI_REDMINE_URL}" == "https://sistemascotec.icmbio.gov.br" ]; then

    echo "jq --raw-output '.memberships[] | select(.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_DESENVOLVEDOR}\"))' memberships.json |  jq '.user.id'" >cmd_ids_EQUIPE_BUILD

  else

    echo "jq --raw-output '.memberships[] | select(.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_DESENVOLVEDOR}\"))' memberships.json |  jq '.user.id'" >cmd_ids_EQUIPE_BUILD

  fi

  chmod +x cmd_ids_EQUIPE_BUILD
  ./cmd_ids_EQUIPE_BUILD

  ids_DEV_BUILD=$(./cmd_ids_EQUIPE_BUILD)

  echo "ids_DEV_BUILD => ${ids_DEV_BUILD}"

  ## declare an array com os jobs que serao dado permissao de BUILD PARA O PERFIL
  #${__PDCI_REDMINE_PERFIL_DESENVOLVEDOR}
  declare -a arr=("pdci-${nome_projeto}-004-customizar-bd-image_file_dump.xml"
    "pdci-${nome_projeto}-020-upd-sql-em-TCTI-db.xml"
    "pdci-${nome_projeto}-021-deploy-em-TCTI-php.xml"
    "pdci-${nome_projeto}-022-deploy-em-TCTI-app-log.xml"
    "pdci-${nome_projeto}-023-em-TCTI-nginx-proxy-log.xml"
    "pdci-${nome_projeto}-030-upd-sql-em-HMG-db.xml"
    "pdci-${nome_projeto}-031-deploy-em-HMG-php.xml"
    "pdci-${nome_projeto}-032-deploy-em-HMG-app-log.xml"
    "pdci-${nome_projeto}-033-em-HMG-nginx-proxy-log.xml"
    "pdci-${nome_projeto}-034-exec-em-HMG.xml"
    "pdci-${nome_projeto}-042-deploy-em-PRD-app-log.xml"
    "pdci-${nome_projeto}-043-em-PRD-nginx-proxy-log.xml"
    "pdci-${nome_projeto}-050-abertura-chamado-deploy-db-PRD.xml"
    "pdci-${nome_projeto}-051-abertura-chamado-deploy-app-PRD.xml"
    "pdci-${nome_projeto}-062-deploy-em-TRN-app-log.xml"
    "pdci-${nome_projeto}-063-docker-em-TRN-app-info.xml"
    "pdci-${nome_projeto}-061-deploy-em-TRN-app.xml"
    "pdci-${nome_projeto}-060-upd-sql-em-TRN-db.xml"
    "pdci-${nome_projeto}-063-em-TRN-nginx-proxy-log.xml"
    "pdci-${nome_projeto}-026-deploy-k8s-TCTI-app.xml"
    "pdci-${nome_projeto}-036-deploy-k8s-HMG-app.xml")

  ## now loop through the above array
  #for i in "${arr[@]}"

  if [ "${ids_DEV_BUILD}" != "" ]; then

    for user_id in ${ids_DEV_BUILD}; do
      echo "===================================================================================================================="
      echo ""
      echo "Buscando dados para user_id => ${user_id}"
      echo "${__PDCI_REDMINE_URL}/users/${user_id}.json"
      user_username=$(curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/users/${user_id}.json | jq --raw-output .user.login)

      curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/users/${user_id}.json | jq --raw-output .user.firstname >${user_id}.firstname.txt
      user_firstname=$(cat ${user_id}.firstname.txt)

      echo "-------------------------------------------------------"
      echo "user_username => ${user_username}"
      for i in "${arr[@]}"; do

        if [[ -f "$i" ]]; then

          echo " =====================================   BUILD DEV ============================================================================="
          echo ""
          echo "${__PDCI_REDMINE_PERFIL_DESENVOLVEDOR} :: Atribuindo permissao de BUILD ao usuario: ${user_username} - ${user_firstname} no job $i ."
          echo ""
          echo ""
          sed -i "s|<inheritanceStrategy class=\"org.jenkinsci.plugins.matrixauth.inheritance.InheritParentStrategy\"\/>|<inheritanceStrategy class=\"org.jenkinsci.plugins.matrixauth.inheritance.InheritParentStrategy\"\/><permission>hudson.model.Item.Build:${user_username}</permission><permission>hudson.model.Item.Cancel:${user_username}</permission><permission>hudson.model.Run.Replay:${user_username}</permission><permission>hudson.model.Run.Update:${user_username}</permission>|g" $i

        fi

      done

    done
  else
    echo ""
    echo "Projeto sem atrubuição no redmine ${__PDCI_REDMINE_URL} de ${__PDCI_REDMINE_PERFIL_DESENVOLVEDOR}"
    echo ""
    exit 1
  fi

  #====================================================PERMISSAO PARA  CONFIGURACAO DE JOBS PARA O PDCI SCRUM ===============================
  echo "__PDCI_REDMINE_URL>>>>${__PDCI_REDMINE_URL}"
  echo "project_id>>>>${project_id}"
  echo "======================PERMISSAO PARA  CONFIGURACAO DE JOBS PARA O PDCI SCRUM  ============"
  echo ""
  echo ""
  curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/projects/${project_id}/memberships.json?limit=200 >memberships.json

  if [ "${PDCI_REDMINE_URL}" == "https://sistemascotec.icmbio.gov.br" ]; then

    echo "jq --raw-output '.memberships[] | select(.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_SCRUM}\"))' memberships.json |  jq '.user.id'" >cmd_ids_EQUIPE_BUILD

  else

    echo "jq --raw-output '.memberships[] | select(.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_SCRUM}\"))' memberships.json |  jq '.user.id'" >cmd_ids_EQUIPE_BUILD

  fi

  chmod +x cmd_ids_EQUIPE_BUILD
  ./cmd_ids_EQUIPE_BUILD

  ids_DEV_BUILD=$(./cmd_ids_EQUIPE_BUILD)

  echo "ids_DEV_BUILD => ${ids_DEV_BUILD}"

  if [ "${ids_DEV_BUILD}" != "" ]; then

    for user_id in ${ids_DEV_BUILD}; do
      echo "===================================================================================================================="
      echo ""
      echo "Buscando dados para user_id => ${user_id}"
      echo "${__PDCI_REDMINE_URL}/users/${user_id}.json"
      user_username=$(curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/users/${user_id}.json | jq --raw-output .user.login)

      curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/users/${user_id}.json | jq --raw-output .user.firstname >${user_id}.firstname.txt
      user_firstname=$(cat ${user_id}.firstname.txt)

      echo "-------------------------------------------------------"
      echo "user_username => ${user_username}"
      for i in "${arr_all_jobs[@]}"; do
        if [[ -f "$i" ]]; then

          echo " =====================================   BUILD DEV ============================================================================="
          echo ""
          echo "${__PDCI_REDMINE_PERFIL_SCRUM} :: Atribuindo permissao de BUILD ao usuario: ${user_username} - ${user_firstname} no job $i ."
          echo ""
          echo ""
          #      <permission>hudson.model.Item.Build:__USER__</permission>
          #      <permission>hudson.model.Item.Cancel:__USER__</permission>
          #      <permission>hudson.model.Item.Configure:__USER__</permission>
          #      <permission>hudson.model.Item.Delete:__USER__</permission>
          #      <permission>hudson.model.Item.Discover:__USER__</permission>
          #      <permission>hudson.model.Item.Move:__USER__</permission>
          #      <permission>hudson.model.Item.Read:__USER__</permission>
          #      <permission>hudson.model.Item.Workspace:__USER__</permission>
          sed -i "s|<inheritanceStrategy class=\"org.jenkinsci.plugins.matrixauth.inheritance.InheritParentStrategy\"\/>|<inheritanceStrategy class=\"org.jenkinsci.plugins.matrixauth.inheritance.InheritParentStrategy\"\/><permission>hudson.model.Item.Workspace:${user_username}</permission><permission>hudson.model.Item.Configure:${user_username}</permission><permission>hudson.model.Item.Build:${user_username}</permission><permission>hudson.model.Item.Cancel:${user_username}</permission><permission>hudson.model.Run.Replay:${user_username}</permission><permission>hudson.model.Run.Update:${user_username}</permission> <permission>hudson.model.Item.Delete:${user_username}</permission>|g" $i

        fi

      done

    done
  else
    echo ""
    echo "Projeto sem atrubuição no redmine ${__PDCI_REDMINE_URL} de ${__PDCI_REDMINE_PERFIL_SCRUM}"
    echo ""
    exit 1
  fi

  #====================================================PERMISSAO DA INFRA PARA DEPLOY EM PRD CONFORME REDMINE ${PDCI_REDMINE_URL_INFRA} ===============================
  echo ""
  echo ""
  echo "============================================PERMISSAO DA INFRA PARA DEPLOY EM PRD CONFORME REDMINE ${PDCI_REDMINE_URL_INFRA} ==============================="
  echo ""
  echo ""
  #declare -a arr=("pdci-${nome_projeto}-041-deploy-em-PRD-php.xml")

  curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_INFRA_SELECIONADO_API_KEY}" ${PDCI_REDMINE_URL_INFRA}/projects/${infra_project_id}/memberships.json?limit=200 >infra_memberships.json

  echo "jq --raw-output '.memberships[] | select(.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_INFRA_DEPLOY}\"))' infra_memberships.json |  jq '.user.id'" >cmd_ids_INFRA_DEPLOY_PRD
  chmod +x cmd_ids_INFRA_DEPLOY_PRD
  ./cmd_ids_INFRA_DEPLOY_PRD

  ids_INFRA_DEPLOY=$(./cmd_ids_INFRA_DEPLOY_PRD)

  echo "ids_INFRA_DEPLOY => ${ids_INFRA_DEPLOY}"

  declare -a arr=("pdci-${nome_projeto}-040-upd-sql-em-PRD-db.xml"
    "pdci-${nome_projeto}-041-deploy-em-PRD-php.xml"
    "pdci-${nome_projeto}-062-deploy-em-TRN-app-log.xml"
    "pdci-${nome_projeto}-061-deploy-em-TRN-app.xml"
    "pdci-${nome_projeto}-062-deploy-em-TRN-app-log.xml"
    "pdci-${nome_projeto}-063-docker-em-TRN-app-info.xml"
    "pdci-${nome_projeto}-060-upd-sql-em-TRN-db.xml"
    "pdci-${nome_projeto}-046-deploy-k8s-PRD-app.xml"
    "pdci-${nome_projeto}-066-deploy-k8s-TRN-app.xml")

  if [ "${ids_INFRA_DEPLOY}" != "" ]; then

    for user_id in ${ids_INFRA_DEPLOY}; do

      echo "==============================="
      echo ""
      echo "Buscando dados para user_id => ${user_id}"
      echo "${PDCI_REDMINE_URL_INFRA}/users/${user_id}.json"
      user_username=$(curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_INFRA_SELECIONADO_API_KEY}" ${PDCI_REDMINE_URL_INFRA}/users/${user_id}.json | jq --raw-output .user.login)

      curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_INFRA_SELECIONADO_API_KEY}" ${PDCI_REDMINE_URL_INFRA}/users/${user_id}.json | jq --raw-output .user.firstname >${user_id}.firstname.txt
      user_firstname=$(cat ${user_id}.firstname.txt)

      echo "-------------------------------------------------------"
      echo "user_username => ${user_username}"
      for i in "${arr[@]}"; do #for i in `ls *deploy-em-PRD*.xml`
        if [[ -f "$i" ]]; then
          echo "${__PDCI_REDMINE_PERFIL_INFRA_DEPLOY} :: Atribuindo permissao ao usuario: ${user_username} - ${user_firstname} no job $i ."
          echo ""
          echo ""
          #      <permission>hudson.model.Item.Cancel:__USER__</permission>
          #      <permission>hudson.model.Item.Configure:__USER__</permission>
          sed -i "s|<inheritanceStrategy class=\"org.jenkinsci.plugins.matrixauth.inheritance.InheritParentStrategy\"\/>|<inheritanceStrategy class=\"org.jenkinsci.plugins.matrixauth.inheritance.InheritParentStrategy\"\/><permission>hudson.model.Item.Configure:${user_username}</permission><permission>hudson.model.Item.Build:${user_username}</permission><permission>hudson.model.Item.Cancel:${user_username}</permission><permission>hudson.model.Run.Replay:${user_username}</permission><permission>hudson.model.Run.Update:${user_username}</permission>|g" $i

        fi
      done

    done
  else
    echo ""
    echo "Projeto sem atrubuição no redmine ${PDCI_REDMINE_URL_INFRA} de ${__PDCI_REDMINE_PERFIL_INFRA_DEPLOY}"
    echo ""
    exit 1
  fi

  #====================================================PERMISSAO DA INFRA PARA UPD DOS SQL EM PRD===============================
  echo ""
  echo ""
  echo "=======================================================PERMISSAO DA INFRA PARA UPD DOS SQL EM PRD CONFORME REDMINE ${PDCI_REDMINE_URL_INFRA} ==============================="
  echo ""
  echo ""
  #relacao das atividades
  declare -a arr=("pdci-${nome_projeto}-040-upd-sql-em-PRD-db.xml"
    "pdci-${nome_projeto}-060-upd-sql-em-TRN-db.xml"
  )

  echo "jq --raw-output '.memberships[] | select(.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_INFRA_BD}\"))' infra_memberships.json |  jq '.user.id'" >cmd_ids_PDCI_INFRA_BD
  chmod +x cmd_ids_PDCI_INFRA_BD
  ./cmd_ids_PDCI_INFRA_BD

  ids_PDCI_INFRA_BD=$(./cmd_ids_PDCI_INFRA_BD)

  echo "ids_PDCI_INFRA_BD => ${ids_PDCI_INFRA_BD}"

  if [ "${ids_PDCI_INFRA_BD}" != "" ]; then

    for user_id in ${ids_PDCI_INFRA_BD}; do

      echo "===================================================================================================================================="
      echo ""
      echo "Buscando dados para user_id => ${user_id}"
      echo "${PDCI_REDMINE_URL_INFRA}/users/${user_id}.json"
      user_username=$(curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_INFRA_SELECIONADO_API_KEY}" ${PDCI_REDMINE_URL_INFRA}/users/${user_id}.json | jq --raw-output .user.login)

      curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_INFRA_SELECIONADO_API_KEY}" ${PDCI_REDMINE_URL_INFRA}/users/${user_id}.json | jq --raw-output .user.firstname >${user_id}.firstname.txt
      user_firstname=$(cat ${user_id}.firstname.txt)

      echo "-------------------------------------------------------"
      echo "user_username => ${user_username}"
      for i in "${arr[@]}"; do #for i in `ls *upd-sql-em-PRD-db.xml`
        if [[ -f "$i" ]]; then
          echo ""
          echo "===================================================================================================================================="
          echo "${__PDCI_REDMINE_PERFIL_INFRA_BD} ::  Atribuindo permissao de UPD SQL EM PRD ao usuario: ${user_username} - ${user_firstname} no job $i ."
          echo "===================================================================================================================================="
          echo ""
          echo ""
          sed -i "s|<inheritanceStrategy class=\"org.jenkinsci.plugins.matrixauth.inheritance.InheritParentStrategy\"\/>|<inheritanceStrategy class=\"org.jenkinsci.plugins.matrixauth.inheritance.InheritParentStrategy\"\/><permission>hudson.model.Item.Configure:${user_username}</permission><permission>hudson.model.Item.Build:${user_username}</permission><permission>hudson.model.Item.Cancel:${user_username}</permission><permission>hudson.model.Run.Replay:${user_username}</permission><permission>hudson.model.Run.Update:${user_username}</permission>|g" $i
        fi
      done

    done

  else

    echo ""
    echo "Projeto sem atrubuição no redmine ${PDCI_REDMINE_URL_INFRA} de ${__PDCI_REDMINE_PERFIL_INFRA_BD}"
    echo ""
    exit 1
  fi

  #Atualizar os jobs do jenkins com os perfis definidos no redmine
  echo ""
  echo ""

  for i in $(ls pdci-*); do

    nome_job_novo=$(echo $i | sed -e "s/.xml//g")
    echo " Atualizando permissoes ao usuario: ${user_username} no job $i ($nome_job_novo) ."
    echo "Criando se não existir: ${nome_job_novo}"
    java -jar jenkins-cli.jar -s ${JENKINS_URL} create-job ${nome_job_novo} <${i} || true
    echo "Atualizando se existir: ${nome_job_novo}"
    java -jar jenkins-cli.jar -s ${JENKINS_URL} update-job ${nome_job_novo} <${i} || true
    echo ""
    echo ""
  done

  #

}

if [ "${PDCI_URL_GITLAB_PROJETO}" == "Atualizar Todos" ]; then

  declare -a arr=("https://gitlab.com/sisicmbio/app_sicae.git"
    "https://gitlab.com/sisicmbio/app_mainapp.git"
    "https://gitlab.com/sisicmbio/app_static_cdn.git"
    "https://gitlab.com/sisicmbio/app_cairu.git"
    "https://gitlab.com/sisicmbio/app_sarr.git"
    "https://gitlab.com/sisicmbio/app_simac.git"
    "https://gitlab.com/sisicmbio/app_infoconv.git"
    "https://gitlab.com/sisicmbio/app_brigadistas.git"
    "https://gitlab.com/sisicmbio/app_teste.git"
    "https://gitlab.com/sisicmbio/app_canie.git"
    "https://gitlab.com/sisicmbio/app_monitora.git"
    "https://gitlab.com/sisicmbio/app_lafsisbio.git")

  ## now loop through the above array
  for i in "${arr[@]}"; do #for i in `ls exemplo-db-pdci-*{db_prd_name}*.xml`
    echo ""
    echo "========================================================================================"
    echo ""
    echo "Projeto $i "
    echo ""
    echo "========================================================================================"

    PDCI_URL_GITLAB_PROJETO=$i

    atualizar_job

  done

else

  atualizar_job

fi

################################################################
#
# Configurar o sentry
#
#################################################################

PDCI_SENTRY_API_KEY=42fe8cf89d2c433c9666669fc91692be28bdeacfa9aa4982b291b3570e6e50dc
PDCI_SENTRY_DSN=https://sentry.icmbio.gov.br
PDCI_SENTRY_ORGANIZATION_NAME=sentry
PDCI_SENTRY_ORGANIZATION_SLUG=sentry
PDCI_SENTRY_TEAM_NAME=sentry
PDCI_SENTRY_TEAM_SLUG=sentry
PDCI_SENTRY_PROJETO_NAME=teste-ciacao-projeto2
PDCI_SENTRY_PROJETO_SLUG=teste-ciacao-projeto2
PDCI_SENTRY_PROJETO_MEMBER_EMAIL=roquebrasilia3@gmail.com
PDCI_SENTRY_PROJETO_MEMBER_USER=roquebrasilia3@gmail.com

if [ "${PDCI_REDMINE_URL}" = "" ]; then
  echo ""
  echo ""
  echo ""
  echo "================ ERROR================="
  echo ""
  echo "Favor informar o parametro PDCI_REDMINE_URL."
  echo ""
  echo ""
  exit 1

fi

#if [ "${PDCI_REDMINE_URL}" == "https://sistemascotec.icmbio.gov.br" ]; then
#
#  __PDCI_REDMINE_API_KEY=${__PDCI_REDMINE_SISTEMASCOTEC_API_KEY}
#
#else
#
#  __PDCI_REDMINE_API_KEY=${__PDCI_REDMINE_GESTAO_API_KEY}
#
#fi
#
#if [ "${__PDCI_REDMINE_API_KEY}" = "" ]; then
#  echo ""
#  echo ""
#  echo ""
#  echo "================ ERROR================="
#  echo ""
#  echo "Favor informar API KEY do usuário no Redmine ${PDCI_REDMINE_URL}, em Configure (Configuração) deste Job."
#  echo ""
#  echo ""
#  exit 1
#
#fi

#ATRIBUIR PERMISSAO A LISTA DE USUARIOS CONFORME PERFIL ATRIBUIDO NO REDMINE
wget https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64
chmod +x jq-linux64
mv jq-linux64 jq
ls -la

#descobrir o id do projeto no redmine.
__PDCI_REDMINE_PERFIL_INFRA_BD=${PDCI_REDMINE_PERFIL_INFRA_BD}
__PDCI_REDMINE_PERFIL_INFRA_DEPLOY=${PDCI_REDMINE_PERFIL_INFRA_DEPLOY}
__PDCI_REDMINE_PERFIL_DESENVOLVEDOR=${PDCI_REDMINE_PERFIL_DESENVOLVEDOR}
__PDCI_REDMINE_PERFIL_SCRUM=${PDCI_REDMINE_PERFIL_SCRUM}
__PDCI_REDMINE_URL=${PDCI_REDMINE_URL}
__PDCI_REDMINE_FIND_GITLAB_PROJECT=${PDCI_REDMINE_FIND_GITLAB_PROJECT}
__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB=${PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}
#__PDCI_REDMINE_API_KEY=${PDCI_REDMINE_API_KEY}

echo "curl -k -v -H  --insecure \"Content-Type: application/json\" -X GET -H \"X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}\" ${__PDCI_REDMINE_URL}/projects.json?limit=200 > projects.json" >comando-project

curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/projects.json?limit=200 >projects.json

if [ "${PDCI_REDMINE_URL}" == "https://sistemascotec.icmbio.gov.br" ]; then

  echo "jq '.projects[] | select( ( .custom_fields[].value  | contains(\"${PDCI_URL_GITLAB_PROJETO}\") ) and  .custom_fields[].name ==\"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" )? '  projects.json | jq --raw-output '.' " >cmd_project_jq

else

  echo "jq '.projects[] | select( .custom_fields[].name  == \"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" and  .custom_fields[].value == \"${PDCI_URL_GITLAB_PROJETO}\"   )? | . '   projects.json  | jq --raw-output '.' " >cmd_project_jq

fi

#echo "jq '.projects[] | select( ( .custom_fields[].value  | contains(\"${PDCI_URL_GITLAB_PROJETO}\") ) and  .custom_fields[].name ==\"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" )? '  projects.json | jq --raw-output '.' " >cmd_project_jq

chmod +x cmd_project_jq
./cmd_project_jq >projects_jq.json

PDCI_SENTRY_PROJETO_NAME=$(jq '.name' projects_jq.json | jq --raw-output '.' | awk '{print tolower($0)}')
PDCI_SENTRY_PROJETO_SLUG=$(jq '.identifier' projects_jq.json | jq --raw-output '.' | awk '{print tolower($0)}')

if [ "${PDCI_SENTRY_PROJETO_NAME}" == "" ]; then
  echo "================ ERROR================="
  echo ""
  echo "Nome do projeto não encontrado no redmine."
  echo ""
  echo ""
  exit 1
fi

if [ "${PDCI_SENTRY_PROJETO_SLUG}" == "" ]; then
  echo "================ ERROR================="
  echo ""
  echo "Identificador do projeto não encontrado no redmine."
  echo ""
  echo ""
  exit 1
fi

echo " PDCI_SENTRY_PROJETO_NAME => ${PDCI_SENTRY_PROJETO_NAME}"
echo " PDCI_SENTRY_PROJETO_SLUG => ${PDCI_SENTRY_PROJETO_SLUG}"

echo ""
echo ""
echo "Integração com Sentry Criando projeto caso não exista"
echo ""
echo ""

#echo "jq '.projects[] | select( ( .custom_fields[].value  | contains(\"${PDCI_URL_GITLAB_PROJETO}\") ) and  .custom_fields[].name ==\"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" )? ' projects.json  | jq --raw-output '.' "
#
#eval "jq '.projects[] | select( ( .custom_fields[].value  | contains(\"${PDCI_URL_GITLAB_PROJETO}\") ) and  .custom_fields[].name ==\"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" )? ' projects.json  | jq --raw-output '.' "

echo "curl ${PDCI_SENTRY_DSN}/api/0/teams/${PDCI_SENTRY_ORGANIZATION_SLUG}/${PDCI_SENTRY_TEAM_SLUG}/projects/ \
  -H 'Authorization: Bearer '${PDCI_SENTRY_API_KEY}'' \
  -H 'Content-Type: application/json' \
  -d '{\"name\":\"'${PDCI_SENTRY_PROJETO_NAME}'\",\"slug\":\"'${PDCI_SENTRY_PROJETO_NAME}'\"}' | jq '.'" >cmd_sentry.sh

##criando projeto no  sentry vinculando ao time.
curl ${PDCI_SENTRY_DSN}/api/0/teams/${PDCI_SENTRY_ORGANIZATION_SLUG}/${PDCI_SENTRY_TEAM_SLUG}/projects/ \
  -H 'Authorization: Bearer '${PDCI_SENTRY_API_KEY}'' \
  -H 'Content-Type: application/json' \
  -d '{"name":"'${PDCI_SENTRY_PROJETO_NAME}'","slug":"'${PDCI_SENTRY_PROJETO_NAME}'"}' |  jq '.' || true
#
##criado a public key do projeto.
#curl ${PDCI_SENTRY_DSN}/api/0/projects/${PDCI_SENTRY_ORGANIZATION_SLUG}/${PDCI_SENTRY_PROJETO_SLUG}/keys/ \
# -H 'Authorization: Bearer '${PDCI_SENTRY_API_KEY}'' \
# -H 'Content-Type: application/json' \
# -d '{"name":"Fabulous Key"}' | jq '.dsn.public'
## adicionar um merge request com a public key

echo ""
echo "Verifica PDCI_REDMINE_URL"
echo ""

if [ "${PDCI_REDMINE_URL}" == "https://sistemascotec.icmbio.gov.br" ]; then



  echo "jq '.projects[] | select( ( .custom_fields[].value  | contains(\"${PDCI_URL_GITLAB_PROJETO}\") ) and  .custom_fields[].name ==\"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" )? '  projects.json | jq --raw-output '.id' " >cmd_project

else

  echo "jq '.projects[] | select( .custom_fields[].name  == \"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" and  .custom_fields[].value == \"${PDCI_URL_GITLAB_PROJETO}\"   )? | . '   projects.json  | jq --raw-output '.id' " >cmd_project

fi

#echo "jq '.projects[] | select( ( .custom_fields[].value  | contains(\"${PDCI_URL_GITLAB_PROJETO}\") ) and  .custom_fields[].name ==\"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" )? '  projects.json | jq --raw-output '.id' " >cmd_project

chmod +x cmd_project
./cmd_project
project_id=$(./cmd_project)

if [ "${project_id}" == "" ]; then

  echo ""
  echo ""
  echo "***********************************   ERRO   ***********************************"
  echo ""
  echo "NAO foi encontrado nenhum projeto no redmine ${__PDCI_REDMINE_URL}"
  echo "    com o campo \"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" contendo a palavra \"${PDCI_URL_GITLAB_PROJETO}\" "
  echo "       nas configuracoes dos projetos."
  echo ""
  echo "================================================================================"
  echo ""
  exit 1
fi

#==================================================Descobrir o project_infra_id no redmine ${PDCI_REDMINE_URL_INFRA} =========================================================================

curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_INFRA_SELECIONADO_API_KEY}" ${PDCI_REDMINE_URL_INFRA}/projects.json?limit=200 >infra_projects.json

if [ "${PDCI_REDMINE_URL}" == "https://sistemascotec.icmbio.gov.br" ]; then

  echo "jq '.projects[] | select( ( .custom_fields[].value  | contains(\"${PDCI_URL_GITLAB_PROJETO}\") ) and  .custom_fields[].name ==\"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" )? '  infra_projects.json | jq --raw-output '.id' " >cmd_infra_project

else

  echo "jq '.projects[] | select( .custom_fields[].name  == \"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" and  .custom_fields[].value == \"${PDCI_URL_GITLAB_PROJETO}\"   )? | . '   infra_projects.json  | jq --raw-output '.id' " >cmd_infra_project

fi

#echo "jq '.projects[] | select( ( .custom_fields[].value  | contains(\"${PDCI_URL_GITLAB_PROJETO}\") ) and  .custom_fields[].name ==\"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" )? '  infra_projects.json | jq --raw-output '.id' " >cmd_infra_project

chmod +x cmd_infra_project
./cmd_infra_project
infra_project_id=$(./cmd_infra_project)

if [ "${infra_project_id}" == "" ]; then

  echo ""
  echo ""
  echo "***********************************   ERRO   ***********************************"
  echo ""
  echo "NAO foi encontrado nenhum projeto no redmine ${PDCI_REDMINE_URL_INFRA}"
  echo "    com o campo \"${__PDCI_REDMINE_CUSTOM_FIELD_PROJECT_GITLAB}\" contendo a palavra \"${PDCI_URL_GITLAB_PROJETO}\" "
  echo "       nas configuracoes dos projetos."
  echo ""
  echo "================================================================================"
  echo ""
  exit 1
fi

#====================================================PERMISSAO DE LEITURA EM TODOS OS JOBS DO PROJETO PARA EQUIPE DE DESENVOLVIMENTO E INFRA ===============================

curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/projects/${project_id}/memberships.json?limit=200 >memberships.json

#===========================================ADICIONADO A EQUIPE DE INFRA
curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_INFRA_SELECIONADO_API_KEY}" ${PDCI_REDMINE_URL_INFRA}/projects/${infra_project_id}/memberships.json?limit=200 >>memberships.json

echo "jq '.memberships[] | select( (.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_INFRA_BD}\")) or (.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_INFRA_DEPLOY}\")) or (.roles[].name|index(\"${__PDCI_REDMINE_PERFIL_DESENVOLVEDOR}\"))  ) ' memberships.json  |  jq --raw-output '.user.id'" >cmd_ids_EQUIPE
#echo "jq --raw-output '.user.id' memberships.json " >cmd_ids_EQUIPE

chmod +x cmd_ids_EQUIPE
./cmd_ids_EQUIPE

ids_EQUIPE=$(./cmd_ids_EQUIPE)

echo "ids_EQUIPE => ${ids_EQUIPE}"

if [ "${ids_EQUIPE}" != "" ]; then

  for user_id in ${ids_EQUIPE}; do
    echo "==============================="
    echo ""
    echo "Buscando dados para user_id => ${user_id}"
    echo "${__PDCI_REDMINE_URL}/users/${user_id}.json"

    curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/users/${user_id}.json >user_${user_id}.json

    #    cat user_${user_id}.json | jq --raw-output '.'

    curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/users/${user_id}.json | jq --raw-output .user.login >${user_id}.user_username.txt
    curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/users/${user_id}.json | jq --raw-output .user.firstname >${user_id}.firstname.txt
    curl -k -v -H --insecure "Content-Type: application/json" -X GET -H "X-Redmine-API-Key: ${__PDCI_REDMINE_API_KEY}" ${__PDCI_REDMINE_URL}/users/${user_id}.json | jq --raw-output .user.mail >${user_id}.mail.txt
    user_username=$(cat ${user_id}.user_username.txt)
    user_firstname=$(cat ${user_id}.firstname.txt)
    PDCI_SENTRY_PROJETO_MEMBER_EMAIL=$(cat ${user_id}.mail.txt)
    echo ""
    echo ""
    echo "-------------------------------------------------------"

    echo "user_username => ${user_username}"
    echo ""
    echo ""
    #adicionado o member no projeto
    curl ${PDCI_SENTRY_DSN}/api/0/organizations/${PDCI_SENTRY_ORGANIZATION_SLUG}/members/ \
      -H 'Authorization: Bearer '${PDCI_SENTRY_API_KEY}'' \
      -H 'Content-Type: application/json' \
      -d '{"email":"'${PDCI_SENTRY_PROJETO_MEMBER_EMAIL}'"
     ,"user":"'${PDCI_SENTRY_PROJETO_MEMBER_EMAIL}'"
     ,"teams":["'${PDCI_SENTRY_TEAM_SLUG}'"]
     ,"role":"member"}' | jq '.'

    echo ""
    echo ""
  done
else
  echo ""
  echo "Projeto sem atrubuição no redmine de ${__PDCI_REDMINE_PERFIL_INFRA_BD} ou ${__PDCI_REDMINE_PERFIL_INFRA_DEPLOY} ou ${__PDCI_REDMINE_PERFIL_DESENVOLVEDOR=PDCI-DESENVOLVEDOR}"
  echo ""
  exit 1
fi
