#!/bin/bash

set -ex
DIR=/backup/sincronizacao_tcti
DATE=`date "+D%y%m%d.H%H%M"`
#LOG_FILE=/usr/local/pgsql/rotinas/logs/sincronizacao_${DATE}.log
LOG_FILE=sincronizacao_${DATE}.log
DB_FILE_RESTORE=docker
DB_NAME_RESTORE=db_restaura_tcti_sisicmbio
DB_NAME_AFTER=db_tcti_sisicmbio
#NOME_ARQUIVO_DUMP_QZ=$(ls /backup/producao/dbprd01/pgsql/${DB_FILE_RESTORE}/${DB_FILE_RESTORE}* | head -1)
executa()
{
## criar diretorio para os indices / SSD conforme conf da infra
#mkdir -p /index_ssd && chown -R postgres:postgres /index_ssd
#mkdir -p /geo && chown -R postgres:postgres /geo

echo ""
echo "Montar pasta /backup"
echo ""

if mount | grep /backup > /dev/null; then
    echo "Pasta /backup já montada."
else
    echo "Inciciando comando de montagem."
    mount -o rw,hard,intr,proto=tcp,port=2049,noatime -t nfs4 10.197.32.35:/backup /backup/
fi


echo ""
echo "Listando conteudo da pasta /backup/producao/dbprd01/pgsql/Dump/docker"
echo ""
ls -lah /backup/producao/dbprd01/pgsql/Dump/docker


#echo ""
#echo "Removendo diretorio $DIR/${DB_FILE_RESTORE}*"
#echo ""
#rm -f $DIR/${DB_FILE_RESTORE}*

sisicmbio=$(/bin/ls -lhr /backup/producao/dbprd01/pgsql/Dump/docker/${DB_FILE_RESTORE}* | awk {'print $9'} | head -1)

#echo ""
#echo "Copiando arquivo $sisicmbio para  $DIR/"
#TEMPO_COPIA_INICIO=$(date "+DATE: %Y-%m-%d TIME: %H:%M:%S")
#echo "${TEMPO_COPIA_INICIO}"
#echo ""
#cp -v $sisicmbio $DIR
#TEMPO_COPIA_TERMINO=$(date "+DATE: %Y-%m-%d TIME: %H:%M:%S")
#echo "${TEMPO_COPIA_TERMINO}"
#echo ""
#echo "Finalizado copia."
#echo ""
#echo ""
#echo "Descompactando "
#echo ""
#gunzip -v ${DIR}/${DB_FILE_RESTORE}*

#echo ""
#echo "Renomeando para  ${DIR}/${DB_FILE_RESTORE}"
#echo ""
#mv ${DIR}/${DB_FILE_RESTORE}* ${DIR}/${DB_FILE_RESTORE}.dump
#
#ls -la ${DIR}/


#NOME_ARQUIVO_DUMP_QZ=$(ls ${DIR}/${DB_FILE_RESTORE}* | head -1)
#echo "NOME_ARQUIVO_DUMP_QZ => ${NOME_ARQUIVO_DUMP_QZ}"

#echo ""
#echo "Marcado apenas para testar a copia do arquivo."
#echo ""
#exit 0;



PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d postgres -c "select pg_terminate_backend(pid) from pg_stat_activity where datname='${DB_NAME_RESTORE}';"
PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d postgres -c "drop database IF EXISTS ${DB_NAME_RESTORE};"
PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d postgres -c "create database ${DB_NAME_RESTORE};"

#echo ""
#echo "Marcado apenas para testar."
#echo ""
#exit 0;

#PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d ${DB_NAME_RESTORE} -c "DROP TABLESPACE  IF EXISTS tbs_index_ssd;";
#PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d ${DB_NAME_RESTORE} -c "CREATE TABLESPACE tbs_index_ssd LOCATION '/index_ssd';";
#PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d ${DB_NAME_RESTORE} -c "DROP TABLESPACE  IF EXISTS tbs_geo;";
#PGPASSWORD=${POSTGRES_DUMP_PASSWORD} PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d ${DB_NAME_RESTORE} -c "CREATE TABLESPACE tbs_geo LOCATION '/geo';";
echo ""
echo "Restaurando dump."
echo ""
TEMPO_INICIO_DA_RESTAURACAO=$(date "+DATE: %Y-%m-%d TIME: %H:%M:%S")
echo "TEMPO INICIO DA RESTAURACAO: ${TEMPO_INICIO_DA_RESTAURACAO}"
echo ""
#PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d ${DB_NAME_RESTORE} -f $DIR/${DB_FILE_RESTORE}.dump
#gunzip -c "${sisicmbio}" | /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d ${DB_NAME_RESTORE} ;
PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d ${DB_NAME_RESTORE} -f $sisicmbio
echo ""
TEMPO_TERMINO_DA_RESTAURACAO=$(date "+DATE: %Y-%m-%d TIME: %H:%M:%S")
echo "*************************************************************************"
echo ""
echo "Inicio da copia do arquivo de dump:  ${TEMPO_COPIA_INICIO}."
echo "Termino da copia do arquivo de dump:  ${TEMPO_COPIA_TERMINO}."
echo "TEMPO INICIO DA RESTAURACAO: ${TEMPO_INICIO_DA_RESTAURACAO}"
echo "TEMPO TERMINO DA RESTAURACAO: ${TEMPO_TERMINO_DA_RESTAURACAO}"
echo ""
echo "*************************************************************************"
echo ""
echo ""
echo "Grant's"
echo ""
PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d postgres -c "GRANT ALL ON DATABASE ${DB_NAME_RESTORE} TO postgres;"
PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d postgres -c "GRANT CONNECT, CREATE ON DATABASE ${DB_NAME_RESTORE} TO usr_jenkins;"

if mount | grep /backup > /dev/null; then
#    ls -la ${DIR}
    echo "Desmontando pasta /backup."
#    rm -f ${DIR}/${DB_FILE_RESTORE}*
#    ls -la ${DIR}
    umount /backup
fi

#

#echo "Realizada a atualicacao com sucesso" >> ${LOG_FILE}


PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d postgres -c "select pg_terminate_backend(pid) from pg_stat_activity where datname='${DB_NAME_RESTORE}';"

echo ""
echo "Habilitado apenas para teste não ira substituir o banco em TCTI"
echo ""
exit 0
PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d postgres -c "select pg_terminate_backend(pid) from pg_stat_activity where datname='${DB_NAME_AFTER}';"
PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d postgres -c "drop database IF EXISTS ${DB_NAME_AFTER};"
PGPASSWORD=${POSTGRES_DUMP_PASSWORD} /usr/local/pgsql/bin/psql -U ${POSTGRES_DUMP_USER} -d postgres -c "ALTER DATABASE ${DB_NAME_RESTORE} RENAME TO ${DB_NAME_AFTER};"


}

executa
#GRANT pg_signal_backend TO usr_jenkinsrestoredump;
GRANT pg_signal_backend,pg_monitor TO usr_jenkins;

#pg_read_all_data
#pg_write_all_data
#pg_read_all_settings
#pg_read_all_stats
#pg_stat_scan_tables
#pg_monitor
#pg_database_owner
#pg_signal_backend
#pg_read_server_files
#pg_write_server_files
#pg_execute_server_program
#pg_checkpoint
#pg_use_reserved_connections
#pg_create_subscription

# pg_read_all_data	Read all data (tables, views, sequences), as if having SELECT rights on those objects, and USAGE rights on all schemas, even without having it explicitly. This role does not have the role attribute BYPASSRLS set. If RLS is being used, an administrator may wish to set BYPASSRLS on roles which this role is GRANTed to.
# pg_write_all_data	Write all data (tables, views, sequences), as if having INSERT, UPDATE, and DELETE rights on those objects, and USAGE rights on all schemas, even without having it explicitly. This role does not have the role attribute BYPASSRLS set. If RLS is being used, an administrator may wish to set BYPASSRLS on roles which this role is GRANTed to.
# pg_read_all_settings	Read all configuration variables, even those normally visible only to superusers.
# pg_read_all_stats	Read all pg_stat_* views and use various statistics related extensions, even those normally visible only to superusers.
# pg_stat_scan_tables	Execute monitoring functions that may take ACCESS SHARE locks on tables, potentially for a long time.
# pg_monitor	Read/execute various monitoring views and functions. This role is a member of pg_read_all_settings, pg_read_all_stats and pg_stat_scan_tables.
# pg_database_owner	None. Membership consists, implicitly, of the current database owner.
# pg_signal_backend	Signal another backend to cancel a query or terminate its session.
# pg_read_server_files	Allow reading files from any location the database can access on the server with COPY and other file-access functions.
# pg_write_server_files	Allow writing to files in any location the database can access on the server with COPY and other file-access functions.
# pg_execute_server_program	Allow executing programs on the database server as the user the database runs as with COPY and other functions which allow executing a server-side program.
# pg_checkpoint	Allow executing the CHECKPOINT command.
# pg_use_reserved_connections	Allow use of connection slots reserved via reserved_connections.
# pg_create_subscription	Allow users with CREATE permission on the database to issue CREATE SUBSCRIPTION.