#!/usr/bin/env bash

if [ "${PDCI_EXEC_DELATHADO}" = "Sim" ]; then
 set -ex
else
 set -e
fi



__GIT_BRANCH=$(echo ${GIT_BRANCH} | tr  '[A-Z]' '[a-z]' | sed -e "s|#|-|g" | sed -e "s|origin\/||g" | sed -e "s|\_|-|g" | sed -e "s|\.|-|g")
#PDCI_GIT_BRANCH=$(echo ${GIT_BRANCH} | tr  '[A-Z]' '[a-z]' | sed -e "s|#|-|g" | sed -e "s|origin\/||g" | sed -e "s|\_|-|g" | sed -e "s|\.|-|g")


#Descobre o nome do projeto.
# Sera o ultimo da url da url do gitlab
v_local_name_project=$(grep -o "[\/]" <<< "$GIT_URL"  | wc -l)
IFS='/' read -ra PARAMETROS <<< "$GIT_URL"
__pdci_nome_projeto=$(echo "${PARAMETROS[${v_local_name_project}]}" | sed -e "s/.git//g" )
__pdci_nome_projeto_mainapp=$(echo "${PARAMETROS[${v_local_name_project}]}" | sed -e "s/.git//g" | sed -e "s/app_//g")

#echo ""
#echo "v_local_name_project=>${v_local_name_project}"
#echo "PDCI_GIT_BRANCH => ${PDCI_GIT_BRANCH}"
#echo "__pdci_nome_projeto_mainapp=>${__pdci_nome_projeto_mainapp}"
#echo "__pdci_nome_projeto=>${__pdci_nome_projeto}"
#echo "GIT_BRANCH => ${GIT_BRANCH}"
#echo ""
#echo "GIT_LOCAL_BRANCH => ${GIT_LOCAL_BRANCH}"
#
#echo "BUILD_NUMBER => ${BUILD_NUMBER}"
#echo "BUILD_ID => ${BUILD_ID}"
#echo "BUILD_DISPLAY_NAME => ${BUILD_DISPLAY_NAME}"


PDCI_APP=${__pdci_nome_projeto_mainapp}
PDCI_HELM_CHART="${PDCI_APP}"
PDCI_HELM_NAME=${PDCI_APP}
PDCI_HELM_DIR=${WORKSPACE}/k8s/helm/${PDCI_APP}
PDCI_HELM_VALUE_TEMPLATE=values.pdci.yaml
PDCI_HELM_VALUE=values.deploy.yaml
PDCI_NAMESPACE=devops
#PATH_KUBECTL=/var/jenkins_home
PATH_KUBECTL=${WORKSPACE}/k8s/helm
PATH_KUBEVAL=${WORKSPACE}/k8s/helm
PATH_MANIFESTO_DEPLOY=${WORKSPACE}/k8s/deploy
PARAM_KUBECONFIG=" --kubeconfig=${KUBECONFIG} "

PDCI_HELM_SET_PARAM_VALUES=" --set pdci.persistentVolume.nfs.server.path=/data-docker-cluster/devops --set pdci.persistentVolume.nfs.server.ip=10.197.93.69 "
PDCI_HELM_NAMESPACE=" --namespace ${PDCI_NAMESPACE} "

cd k8s/helm

export PDCI_FILE_ENV_DOCKER=../../.env.docker.k8s.prd

if [[ !  -f  ${PDCI_FILE_ENV_DOCKER} ]]; then
  export PDCI_FILE_ENV_DOCKER=../../.env.docker.prd
fi



#verficiar se existe as variaveis obrigatorias para o deploy foram informadas no env.prd.

if [[ ! -f  ${PDCI_FILE_ENV_DOCKER} ]]; then

  echo ""
  echo "===================================== ERRO =================================================="
  echo ""
  echo "Arquivo de variáveis de ambiente ( ${PDCI_FILE_ENV_DOCKER} ) do docker da aplicação. "
  echo ""
  echo "============================================================================================="
  exit 1
fi

#projetos que não usam banco de dados.

echo "$GIT_URL => ${GIT_URL}"
#declare -a projetos_skip_bd=("exemplo-pdci-{app_nome}-004-customizar-bd-image_file_dump.xml")

case ${GIT_URL} in
    *"/sisicmbio/pdci/monitorsis.git"*)
        echo ""
        echo " Projeto não utiliza banco de dados."
        echo ""
        ;;
     *)
        echo ""
        echo " Verificação da existência de configuração de banco de dados __pdci_set_db_password_pass_${__pdci_nome_projeto_mainapp} ."
        echo ""

        # comando grep procura pela palavra $word no arquivo $file
        # > /dev/null redireciona a saída do comando grep
        if grep "\${__pdci_set_db_password_pass_${__pdci_nome_projeto_mainapp}}" ${PDCI_FILE_ENV_DOCKER} > /dev/null
        then
          echo ""
          echo "A palavra \${__pdci_set_db_password_pass_${__pdci_nome_projeto}} existe no arquivo ${PDCI_FILE_ENV_DOCKER} ."
          echo ""
        else
          echo ""
          echo "===================================== ERRO ============================================================"
          echo ""
          echo " Favor informar a variável PDCI_DB_PASSWORD='\${__pdci_set_db_password_pass_${__pdci_nome_projeto}}' "
          echo "    no arquivo ${PDCI_FILE_ENV_DOCKER} ."
          echo ""
          echo "======================================================================================================="
          echo ""
          exit 1
        fi

       declare -a arr_mandadory_variable_env=("PDCI_DB_NAME"
        "PDCI_DB_HOST"
        "PDCI_DB_PORT"
        "PDCI_DB_SCHEMA"
        "PDCI_DB_USER"
        "PDCI_DB_PASSWORD"
        "PDCI_DEPLOY_DEVELOPMENT");

       export

        for env in "${arr_mandadory_variable_env[@]}"; do

        eval 'unset '${env}''

        done;

        export

        source ${PDCI_FILE_ENV_DOCKER}

        export


        for env in "${arr_mandadory_variable_env[@]}"; do
        eval 'if [   -z $'"${env}"' ]; then
            echo ""
            echo "===================================== ERRO =================================="
            echo ""
            echo "A variável ${env} deve ser adicionada no arquivo ${PDCI_FILE_ENV_DOCKER}."
            echo ""
            echo "============================================================================="
            exit 1
          else
            echo " Variável obrigatória ${env} foi encontrada no ${PDCI_FILE_ENV_DOCKER} . "
          fi'

        done;




        printenv |  sed 's/\x0D$//'  | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '__pdci_set_' | cut -d'=' -f1 > var_pdci_set

        cat var_pdci_set

          for v_i in `cat var_pdci_set`
          do
            # echo "Verificar se foi adicionado no template do job a variavel solicitada no arquivo ${PDCI_FILE_ENV_DOCKER}  (${v_i})"
              if [ -z $"{${v_i}+x}" ]; then
                echo ""
                echo "===================================== ERRO ===================================================================================================="
                echo ""
                echo "A variável ${v_i} deve ser adicionada ao template deste job pois o mesmo esta sendo requerida pelo arquivo ${PDCI_FILE_ENV_DOCKER}."
                echo ""
                echo "Não esqueca de verficar da existencia do valor correspondente no cofre do jenkins."
                echo ""
                echo "================================================================================================================================================="
                exit 1
              else
              echo ""
              echo " Identificado o uso ${v_i} no arquivo ${PDCI_FILE_ENV_DOCKER}";
              echo ""
              fi
          done


        #printenv |  sed 's/\x0D$//'  | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep "__pdci_set_db_password_pass_${PDCI_APP}" | cut -d'=' -f1 | sed 's/__pdci_set_/${__pdci_set_/g' | awk '{print $0 "}"'}


        if [ -z $"{__pdci_set_db_password_pass_${PDCI_APP}+x}" ]; then
          echo ""
          echo "===================================== ERRO ================================================================================"
          echo ""
          echo "A variavel __pdci_set_db_password_pass_${PDCI_APP} deve ser registrada no cofre do jenkins com a senha do banco de dados"
          echo ""
          echo "==========================================================================================================================="
          exit 1
        #else
        #echo "var is set to '__pdci_set_db_password_pass_${PDCI_APP}'";
        fi

        #verificar se existe mais variaveis do tipo __pdci_set_* que devem ser registrada no cofre e adicionada no template.

        rm -rf value.yaml

        ##instalacao do kubectl
        if [ ! -f  "${PATH_KUBECTL}/kubectl" ]; then
          curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.16.0/bin/linux/amd64/kubectl
          chmod 777 "${PATH_KUBECTL}/kubectl"
          chmod +x  "${PATH_KUBECTL}/kubectl"
        fi
        #
        ###instalacao do kubeval
        ###Verificar sintaxe e boas prativas.
        #if [ ! -f  "${PATH_KUBEVAL}/kubeval" ]; then
        #  wget https://github.com/instrumenta/kubeval/releases/latest/download/kubeval-linux-amd64.tar.gz
        #  tar xf kubeval-linux-amd64.tar.gz
        #  #sudo cp kubeval /usr/local/bin
        #  chmod 777 "${PATH_KUBEVAL}/kubeval"
        #  chmod +x  "${PATH_KUBEVAL}/kubeval"
        #fi



        if [ ! -d  ${PDCI_HELM_DIR} ]; then

          echo ""
          echo "===================================== ERRO =================================================="
          echo ""
          echo "Diretório do Chart do projeto não existe. ( ${PDCI_HELM_DIR} ) "
          echo ""
          echo "============================================================================================="
          exit 1
        fi

        echo "" >> ${PDCI_FILE_ENV_DOCKER}

        echo "PDCI_APP_VERSAO=${__GIT_BRANCH}.${BUILD_NUMBER}" >> ${PDCI_FILE_ENV_DOCKER}


        if [ ! -f  ${PDCI_HELM_DIR}/${PDCI_HELM_VALUE_TEMPLATE} ]; then

          echo ""
          echo "===================================== ERRO ======================================================================="
          echo ""
          echo "Falta o arquivo value definido no PDCI (${PDCI_HELM_DIR}/${PDCI_HELM_VALUE_TEMPLATE}) no projeto para processo de deploy."
          echo ""
          echo "=================================================================================================================="
          exit 1
        fi

        if [ ! -f  ${PDCI_HELM_DIR}/Chart.yaml ]; then

          echo ""
          echo "===================================== ERRO ======================================================================="
          echo ""
          echo "Falta o arquivo Chart do Helm  (${PDCI_HELM_DIR}/Chart.yaml) no projeto para processo de deploy."
          echo ""
          echo "=================================================================================================================="
          exit 1
        fi

        wget -q -O ${WORKSPACE}/yq $(wget -q -O - https://api.github.com/repos/mikefarah/yq/releases/latest | jq -r '.assets[] | select(.name == "yq_linux_amd64") | .browser_download_url') \
        && chmod +x ${WORKSPACE}/yq

        PDCI_CHART_NAME=$(yq e '.name' ${PDCI_HELM_DIR}/Chart.yaml)

        if [ "${PDCI_CHART_NAME}" = "" ]; then

          echo ""
          echo "===================================== ERRO ======================================================================="
          echo ""
          echo "Obrigatório informar o nome do Chart em  (${PDCI_HELM_DIR}/Chart.yaml) ."
          echo ""
          echo "=================================================================================================================="
          exit 1
        fi



        echo ""
        echo "============================================================================================="
        echo ""
        echo " Setando os valores das variaveis  __pdci_set_"
        echo ""
        echo "============================================================================================="



        v_p=${PDCI_FILE_ENV_DOCKER}
        if [ -f "${v_p}" ] ; then
          echo ""
          echo "=========================================== =================================================="
          echo ""
          echo " Setando PDCI variables (__pdci_set_) no arquivo ${v_p}. "
          echo ""
          echo "============================================================================================="
          v_pdci=''
          for v_i in `printenv |  sed 's/\x0D$//'  | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '__pdci_set_' | cut -d'=' -f1 | sed 's/__pdci_set_/${__pdci_set_/g' | awk '{print $0 "}"'}`
          do
             echo "Discovery PDCI Variable  ${v_i} "
             if [ "${v_pdci}" != "" ]; then
             v_pdci="${v_pdci},"
             fi
             v_pdci="${v_pdci}  ${v_i} "
          done
          echo ""
          echo "Replace PDCI variables."
          echo ""
          cp -f -v  ${v_p} ${v_p}.bkp
          #echo "v_envsubst_add"
          v_envsubst="envsubst ' ${v_pdci}' < \"${v_p}.bkp\" > \"${v_p}\""
          #echo "v_envsubst=> ${v_envsubst}"
          eval ${v_envsubst}

        else
          echo "Arquivo ENV.DOCKER não existe. ${v_p}"
        fi



        source ${PDCI_FILE_ENV_DOCKER}

        cp ${PDCI_FILE_ENV_DOCKER} ${PDCI_FILE_ENV_DOCKER}.env_tmp

        egrep -v "^#|^$" ${PDCI_FILE_ENV_DOCKER}.env_tmp > ${PDCI_FILE_ENV_DOCKER}.env


        export $(cut -d= -f1 ${PDCI_FILE_ENV_DOCKER}.env )

    #    export

        if [ -z "${PDCI_PERSISTENTVOLUME_NFS_SERVER_PATH+x}" ]; then
          echo ""
          echo "===================================== ERRO ================================================================================"
          echo ""
          echo "A variável PDCI_PERSISTENTVOLUME_NFS_SERVER_PATH deve ser informada em ${PDCI_FILE_ENV_DOCKER} com o caminho do NFS."
          echo ""
          echo ""
          echo "Exemplo: "
          echo ""
          echo "PDCI_PERSISTENTVOLUME_NFS_SERVER_PATH='/data-docker-cluster/devops'"
          echo ""
          echo "==========================================================================================================================="
          exit 1
        fi

        if [ -z "${PDCI_PERSISTENTVOLUME_NFS_SERVER_IP+x}" ]; then
          echo ""
          echo "===================================== ERRO ================================================================================"
          echo ""
          echo "A variável PDCI_PERSISTENTVOLUME_NFS_SERVER_IP deve ser informada em ${PDCI_FILE_ENV_DOCKER} com o IP do NFS."
          echo ""
          echo ""
          echo "Exemplo: "
          echo ""
          echo "PDCI_PERSISTENTVOLUME_NFS_SERVER_IP='10.197.93.69'"
          echo ""
          echo "==========================================================================================================================="
          exit 1
        fi

        ;;
esac


${PATH_KUBECTL}/kubectl  --kubeconfig=${KUBECONFIG} create namespace ${PDCI_NAMESPACE} || true


export PDCI_GIT_BRANCH=$(echo ${__PDCI_RELEASE:-develop} | tr  '[A-Z]' '[a-z]' | sed -e "s|#|-|g" | sed -e "s|origin\/||g" | sed -e "s|\_|-|g" | sed -e "s|\.|-|g")



echo ""
echo "PDCI_GIT_BRANCH => ${PDCI_GIT_BRANCH}"
echo ""

echo ""
echo "============================================================================================="
echo ""
echo "    Exportando variaveis do arquivo  ${PDCI_FILE_ENV_DOCKER} "
echo ""
echo "============================================================================================="

rm -rf ${PWD}/${PDCI_APP}/Chart.bkp.yaml && \
cp -v ${PWD}/${PDCI_APP}/Chart.yaml ${PWD}/${PDCI_APP}/Chart.bkp.yaml && \
envsubst  < "${PWD}/${PDCI_APP}/Chart.bkp.yaml" > "${PWD}/${PDCI_APP}/Chart.yaml"

cat "${PWD}/${PDCI_APP}/Chart.yaml"





rm -rf ${PDCI_APP}/${PDCI_HELM_VALUE_TEMPLATE}.bkp && \
cp -v ${PDCI_APP}/${PDCI_HELM_VALUE_TEMPLATE} ${PDCI_APP}/${PDCI_HELM_VALUE_TEMPLATE}.bkp && \
envsubst  < "${PWD}/${PDCI_APP}/${PDCI_HELM_VALUE_TEMPLATE}.bkp" > "${PWD}/${PDCI_APP}/${PDCI_HELM_VALUE}"







${PATH_KUBECTL}/kubectl --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} delete configmap ${PDCI_CHART_NAME}-docker-env || true


${PATH_KUBECTL}/kubectl --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG}  \
create configmap ${PDCI_CHART_NAME}-docker-env \
--from-env-file=${PDCI_FILE_ENV_DOCKER} \
-o yaml |${PATH_KUBECTL}/kubectl label -f- \
-o yaml --local app.kubernetes.io/instance=${PDCI_CHART_NAME}

#app.kubernetes.io/name=sisicmbio \

#\
#app.kubernetes.io/managed-by=Helm



#cat ${PWD}/${PDCI_APP}/${PDCI_HELM_VALUE}

cat ${PDCI_APP}/${PDCI_HELM_VALUE}

if [ -f ${PDCI_APP}/Chart.lock ]; then

  echo ""
  echo "============================================================================================="
  echo ""
  echo "    Atualizando dependencias do chart com o Helm "
  echo ""
  echo "============================================================================================="

  helm dependency update ${PDCI_APP}

  ls -la ${PDCI_APP}

else

  echo ""
  echo "============================================================================================="
  echo ""
  echo "    Construindo dependencias do chart com o Helm "
  echo ""
  echo "============================================================================================="

  helm dependency build ${PDCI_HELM_CHART}

   ls -la ${PDCI_HELM_CHART}

fi


echo ""
echo "============================================================================================="
echo ""
echo "    Manifesto do deploy com o Helm "
echo ""
echo "============================================================================================="

helm install ${PDCI_HELM_NAME} ${PDCI_HELM_CHART} \
${PARAM_KUBECONFIG} \
${PDCI_HELM_NAMESPACE} \
--values ${PDCI_APP}/${PDCI_HELM_VALUE} \
${PDCI_HELM_SET_PARAM_VALUES} \
--dry-run --debug

echo ""
echo "============================================================================================="
echo ""
echo "    Realizando deploy com o chart do Helm "
echo ""
echo "============================================================================================="

if [ "${PDCI_HELM_UNINSTALL}" = "Sim" ]; then

helm uninstall ${PDCI_HELM_NAME} ${PARAM_KUBECONFIG} ${PDCI_HELM_NAMESPACE}
sleep 300

fi


helm upgrade --install ${PDCI_HELM_NAME}  ${PDCI_HELM_CHART} ${PARAM_KUBECONFIG} ${PDCI_HELM_NAMESPACE} \
--set app-version=${PDCI_GIT_BRANCH} \
--values ${PDCI_APP}/${PDCI_HELM_VALUE} \
${PDCI_HELM_SET_PARAM_VALUES} \
--wait \
--timeout 10m


echo ""
echo "*****************************************"
echo "events"
echo ""
${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} get events
echo ""
echo "*****************************************"
echo "pv"
echo ""
${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} get pv
echo ""
echo "*****************************************"
echo "pvc"
echo ""
${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} get pvc
echo ""
echo "*****************************************"
echo "service"
echo ""
${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} get service
echo ""
echo "*****************************************"
echo "deployment"
echo ""
${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} get deployment
echo ""
echo "*****************************************"
echo "Describe deployment"
echo ""
${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} describe deployment
echo ""
echo "*****************************************"
echo "pods"
echo ""
${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} get pods
echo ""
echo "*****************************************"
echo "Describe pods"
echo ""
${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} describe pods
echo ""
echo "*****************************************"
echo "endpoints"
echo ""
${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} get endpoints
echo ""
echo "*****************************************"
echo "ingress"
echo ""
${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} get ingress
echo ""
echo "*****************************************"
echo "services"
echo ""
${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} get svc
echo ""
echo "*****************************************"
echo "nodes"
echo ""
${PATH_KUBECTL}/kubectl  --kubeconfig=${KUBECONFIG}  -l name=prod  top  node
echo ""
echo "*****************************************"
echo "Describe nodes"
echo ""
${PATH_KUBECTL}/kubectl  --kubeconfig=${KUBECONFIG}  -l name=prod  describe  node


#${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} logs -f  --tail=-1

#${PATH_KUBECTL}/kubectl -l app.kubernetes.io/instance=${PDCI_APP} --namespace=${PDCI_NAMESPACE}  --kubeconfig=${KUBECONFIG} exec --stdin --tty {pod} -- /bin/bash

unset $(sed 's/\x0D$//'  ${PDCI_FILE_ENV_DOCKER} | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '_REGISTRY' | sed -E 's/(.*)=.*/\1/' | xargs)