#!/bin/bash -ex
export
#JENKINS_URL=${JENKINS_URL:-http://${pdci_suite_jenkins_domain}:8080}
JENKINS_URL=http://127.0.0.1:8080
export TZ='America/Sao_Paulo'
data=$(date +%F_%H:%M:%S)
#IFS for jobs with spaces.
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
echo ${JENKINS_URL}
wget --no-check-certificate  ${JENKINS_URL}/jnlpJars/jenkins-cli.jar
rm -rf jenkins-cli.jar.*


export JENKINS_USER_ID=${__JENKINS_USER_ID}
export JENKINS_API_TOKEN=${__JENKINS_API_TOKEN}


echo "${__JENKINS_USER_ID}" > teste.id
echo "${__JENKINS_API_TOKEN}" > teste.token

declare -a arr=("010-build"
"010-build-develop"
"010-build-feature"
"010-build-master"
"010-build-php"
"010-build-release"
"010-build-release-candidate"
"011-deploy-em-teste-db"
"012-backup-esquema-de-teste-db"
"012-restaura-esquema-de-teste-db"
"012-upd-sql-em-teste-db"
"013-deploy-em-teste-php"
"014-sonar"
"015-teste-cobertura"
"016-teste-selenium")

for i in "${arr[@]}"; do
  for j in $(java -jar jenkins-cli.jar -s ${JENKINS_URL} list-jobs | grep "${i}");
  do
    echo ""
    echo "Deletando job:"
    echo "${j}"
    echo ""
   java -jar jenkins-cli.jar -s ${JENKINS_URL} delete-job ${j} ;
  done
done
