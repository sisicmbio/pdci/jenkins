#!/bin/bash -ex
export
#JENKINS_URL=${JENKINS_URL:-http://${pdci_suite_jenkins_domain}:8080}
JENKINS_URL=http://127.0.0.1:8080
export TZ='America/Sao_Paulo'
data=$(date +%F_%H:%M:%S)
git branch Feature-pdci-jobs_${JOB_NAME}_Build-${BUILD_NUMBER}
git checkout Feature-pdci-jobs_${JOB_NAME}_Build-${BUILD_NUMBER}
#IFS for jobs with spaces.
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
echo ${JENKINS_URL}
wget --no-check-certificate  ${JENKINS_URL}/jnlpJars/jenkins-cli.jar
rm -rf jenkins-cli.jar.*


export JENKINS_USER_ID=${__JENKINS_USER_ID}
export JENKINS_API_TOKEN=${__JENKINS_API_TOKEN}


echo "${__JENKINS_USER_ID}" > teste.id
echo "${__JENKINS_API_TOKEN}" > teste.token

git config --global user.email "roquebrasilia@gmail.com"
git config --global user.name "Rafael Roque de Mello"

git branch -a
git status

#mkdir -p bkp_jobs

#for i in $(java -jar jenkins-cli.jar -s ${JENKINS_URL} list-jobs);
#do
#  java -jar jenkins-cli.jar -s ${JENKINS_URL} get-job ${i} > bkp_jobs/${i}.xml;
#done
mkdir -p bkp_jobs


for i in $(java -jar jenkins-cli.jar -s ${JENKINS_URL} list-jobs | grep k8s);
do
  java -jar jenkins-cli.jar -s ${JENKINS_URL} get-job ${i} > bkp_jobs/${i}.xml;
done


if [ "$(git status | grep "what will be committed")" != "" ]; then

  git add .

  git commit -m "${_PDCI_GITLAB_TITLE_MSG} :: bkp dos jobs k8s  em ${data} :: Build #${BUILD_NUMBER} ."
fi


#Backup exemplo de BD -auth 69974349168:cliente who-am-i

#mkdir -p credentials

#java -jar jenkins-cli.jar  -s ${JENKINS_URL} list-credentials system::system::jenkins

#java -jar jenkins-cli.jar  -s ${JENKINS_URL} get-credentials-as-xml  system::system::jenkins _ 1b9116f9-75c3-4cf2-9eab-3580ab9ada05 > 69974349168.xml


for i in $(java -jar jenkins-cli.jar -s ${JENKINS_URL} list-jobs | grep dev-exemplo);
do
  java -jar jenkins-cli.jar -s ${JENKINS_URL} get-job ${i} > ${i}.xml;
done



for i in $(java -jar jenkins-cli.jar -s ${JENKINS_URL} list-jobs | grep exemplo-db-pdci);
do
  java -jar jenkins-cli.jar -s ${JENKINS_URL} get-job ${i} > ${i}.xml;
done

#Backup exemplo de app
#java -jar /run/jenkins/war/WEB-INF/jenkins-cli.jar -s ${JENKINS_URL} list-jobs
for i in $(java -jar jenkins-cli.jar -s ${JENKINS_URL} list-jobs | grep exemplo-pdci);
do
  java -jar jenkins-cli.jar -s ${JENKINS_URL} get-job ${i} > ${i}.xml;
done
IFS=$SAVEIFS
#tar cvfj "jenkins-jobs-exemplo-pdci.tar.bz2" ./*.xml



#status_git=$(git status | grep "to update what will be committed")
#echo "status_git=>'$(git status | grep "to update what will be committed")'"

if [ "$(git status | grep "what will be committed")" != "" ]; then

  git add .

  git commit -m "${_PDCI_GITLAB_TITLE_MSG} :: templates dos jobs alterados  em ${data} :: Build #${BUILD_NUMBER} ."
fi

#Realizar Backup da lista de blugins
#java -jar jenkins-cli.jar -s ${JENKINS_URL} groovy = < pluginEnumerator.groovy

echo "def plugins = jenkins.model.Jenkins.instance.getPluginManager().getPlugins()" > ${WORKSPACE}/plugins.groovy
echo 'plugins.each {println "${it.getShortName()}:${it.getVersion()}"}' >> ${WORKSPACE}/plugins.groovy

#java -jar jenkins-cli.jar -s http://localhost:8080 groovy --username "admin" --password "admin" = < plugins.groovy > plugins.txt



#status_git=$(git status | grep "to update what will be committed")
#echo "status_git=>'$(git status | grep "to update what will be committed")'"

#desabilitado temporarioamente
java -jar jenkins-cli.jar -s ${JENKINS_URL} groovy = < plugins.groovy > plugins.txt
if [ "$(git status | grep "what will be committed")" != "" ]; then

  git add .

  git commit -m "${_PDCI_GITLAB_TITLE_MSG} :: Relacao de plugins alterados em ${data} :: Build #${BUILD_NUMBER} :: ${JOB_URL} ."
fi

git status

cat plugins.txt



#mkdir -p views

#java -jar jenkins-cli.jar -s ${JENKINS_URL}  get-view exemplo_nome_projeto > views/exemplo_nome_projeto.xml;
#java -jar jenkins-cli.jar -s ${JENKINS_URL}  get-view exemplos-pdci > views/exemplos-pdci.xml;
#java -jar jenkins-cli.jar -s ${JENKINS_URL}  get-view adm_pdci_suite > views/adm_pdci_suite.xml;
java -jar jenkins-cli.jar -s ${JENKINS_URL}  get-view adm_dev > views/adm_dev.xml;
if [ "$(git status | grep "what will be committed")" != "" ]; then

  git add .

  git commit -m "Alteracao na views adm_dev em ${data} :: Build #${BUILD_NUMBER} :: ${JOB_URL} ."
fi
java -jar jenkins-cli.jar -s ${JENKINS_URL}  get-view adm_dba > views/adm_dba.xml;
if [ "$(git status | grep "what will be committed")" != "" ]; then

  git add .

  git commit -m "Alteracao na views adm_dba em ${data} :: Build #${BUILD_NUMBER} :: ${JOB_URL} ."
fi
java -jar jenkins-cli.jar -s ${JENKINS_URL}  get-view adm_pdci_jobs > views/adm_pdci_jobs.xml;
if [ "$(git status | grep "what will be committed")" != "" ]; then

  git add .

  git commit -m "Alteracao na views adm_pdci_jobs em ${data} :: Build #${BUILD_NUMBER} :: ${JOB_URL} ."
fi
java -jar jenkins-cli.jar -s ${JENKINS_URL}  get-view k8s > views/k8s.xml;


if [ "$(git status | grep "what will be committed")" != "" ]; then

  git add .

  git commit -m "Alteracao na views k8s em ${data} :: Build #${BUILD_NUMBER} :: ${JOB_URL} ."
fi


java -jar jenkins-cli.jar -s ${JENKINS_URL}  help
#Realizar Backup do arquivo de configuracao do jenkins

#http://admin:token@JENKINS_URL:8080/pluginManager/api/json?depth=1&?xpath=/*/*shortName|/*/*/version'



