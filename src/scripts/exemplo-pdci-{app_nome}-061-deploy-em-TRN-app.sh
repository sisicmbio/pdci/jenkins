#!/usr/bin/env bash
set -e

#export __PDCI_DOCKER_HOST_TRN=tcp://10.197.32.80:2375
export __PDCI_DOCKER_HOST_TRN=tcp://10.197.93.75:2375
export __PDCI_APPLICATION_ENV=trn
export PDCI_FILE_ENV_DOCKER=./.env.docker.trn

if [ ! -f  ${PDCI_FILE_ENV_DOCKER} ]; then

  echo ""
  echo "===================================== ERRO =================================================="
  echo ""
  echo "Arquivo de variáveis de ambiente ( ${PDCI_FILE_ENV_DOCKER} ) do docker da aplicação. "
  echo ""
  echo "============================================================================================="
  exit 1
fi

v_p=${PDCI_FILE_ENV_DOCKER}
if [ -f "${v_p}" ] ; then
  echo "Setando PDCI variables no arquivo ${v_p}."
  v_pdci=''
  for v_i in `printenv |  sed 's/\x0D$//'  | sed 's/^M$//'  |  sed '/^$/d' | grep -v '^#' | grep '__pdci_set_' | cut -d'=' -f1 | sed 's/__pdci_set_/${__pdci_set_/g' | awk '{print $0 "}"'}`
  do
     echo "Discovery PDCI Variable  ${v_i} "
     if [ "${v_pdci}" != "" ]; then
     v_pdci="${v_pdci},"
     fi
     v_pdci="${v_pdci}  ${v_i} "
  done

  echo "Replace PDCI variables."

  cp -f -v  ${v_p} ${v_p}.bkp
  #echo "v_envsubst_add"
  v_envsubst="envsubst ' ${v_pdci}' < \"${v_p}.bkp\" > \"${v_p}\""
  echo "v_envsubst=> ${v_envsubst}"
  eval ${v_envsubst}

else
  echo "Arquivo ENV.DOCKER não existe. ${v_p}"
fi

PDCI_GIT_BRANCH=$(echo ${__PDCI_RELEASE_CANDIDATE} | tr  '[A-Z]' '[a-z]' | sed -e "s|#|-|g" | sed -e "s|origin\/||g" | sed -e "s|\_|-|g" | sed -e "s|\.|-|g")

sed -i "s|[\$]{PDCI_APPLICATION_ENV}||g" docker-compose.yml
sed -i "s|[\$]{PDCI_GIT_BRANCH}|${PDCI_GIT_BRANCH}|g" docker-compose.yml

v_local_name_project=$(grep -o "[\/]" <<< "$GIT_URL"  | wc -l)
IFS='/' read -ra PARAMETROS <<< "$GIT_URL"
__pdci_nome_projeto_mainapp=$(echo "${PARAMETROS[${v_local_name_project}]}" | sed -e "s/.git//g" | sed -e "s/app_//g")
__pdci_nome_projeto=$(echo "${PARAMETROS[${v_local_name_project}]}" | sed -e "s/.git//g")

echo ""
echo ""
echo "GIT_BRANCH => ${GIT_BRANCH}"
echo ""
echo "GIT_LOCAL_BRANCH => ${GIT_LOCAL_BRANCH}"

echo "BUILD_NUMBER => ${BUILD_NUMBER}"
echo "BUILD_ID => ${BUILD_ID}"
echo "BUILD_DISPLAY_NAME => ${BUILD_DISPLAY_NAME}"

echo ""
echo ""
rm -rf .env
cp ${PDCI_FILE_ENV_DOCKER} .env

echo ""
echo "setando variaveis de usuario de conexao do banco de dados "
echo ""

__pdci_set_db_username_=$( export | grep '__pdci_set_db_username_' | sed 's|declare -x ||g' | sed 's|"||g')

for line in ${__pdci_set_db_username_} ;
do

#echo " >>> $line" ;

IFS='=' read -ra PARAMETROS <<< "$line"

 sed -i "s|${PARAMETROS[0]}|${PARAMETROS[1]}|g" ./.env

done

echo ""
echo ""
echo "setando variaveis senhas de banco pdci"
echo ""

__pdci_set_db_password_=$( export | grep '__pdci_set_db_password_' | sed 's|declare -x ||g' | sed 's|"||g')

for line in ${__pdci_set_db_password_} ;
do

#echo " >>> $line" ;

IFS='=' read -ra PARAMETROS <<< "$line"

#echo ""
#echo "${PARAMETROS[0]}, ${PARAMETROS[1]}, ${PARAMETROS[2]}"

 sed -i "s|${PARAMETROS[0]}|${PARAMETROS[1]}|g" ./.env

done


echo ""

#cat .env | while read line
#do
#    echo " export $line" >> export.env
#done
#export
echo ""
echo ""
#cat export.env
echo ""
echo ""
#source export.env
echo ""


if [ -e docker-compose.yml ]; then
echo ""
	echo "Processando docker-compose.yml"
echo ""
else
  echo ""
  echo "ERRO:  Arquivo docker-compose.yml não existe"
  echo ""
  exit 1
fi;

if [ -e "${PDCI_FILE_ENV_DOCKER}" ]; then
echo ""
	echo "Processando ${PDCI_FILE_ENV_DOCKER}"
echo ""
else
  echo ""
  echo " ERRO: Arquivo ${PDCI_FILE_ENV_DOCKER} não existe"
  echo ""
  exit 1
fi;

deploy_service() {

  echo ""
  docker  -H ${__PDCI_DOCKER_HOST_TRN} network create pdci_${__PDCI_APPLICATION_ENV}_network || true
  echo ""
  echo ""
  docker  -H ${__PDCI_DOCKER_HOST_TRN} volume create pdci_${__PDCI_APPLICATION_ENV}_session_php || true
  echo ""
  echo ""
  docker  -H ${__PDCI_DOCKER_HOST_TRN} volume create pdci_${__PDCI_APPLICATION_ENV}_data_${__pdci_nome_projeto} || true
  echo ""
  echo ""
  docker-compose  -H ${__PDCI_DOCKER_HOST_TRN} --no-ansi -p pdci_${__PDCI_APPLICATION_ENV} -f  docker-compose.yml config
  echo ""
  echo ""
  docker-compose  -H ${__PDCI_DOCKER_HOST_TRN} --no-ansi -p pdci_${__PDCI_APPLICATION_ENV} -f  docker-compose.yml pull
  echo ""
  echo "Removendo container ${__pdci_nome_projeto_mainapp} ..."
  echo ""
  docker-compose  -H ${__PDCI_DOCKER_HOST_TRN} --no-ansi -p pdci_${__PDCI_APPLICATION_ENV} -f  docker-compose.yml rm -f -s ${__pdci_nome_projeto_mainapp}
  echo ""
  echo ""
  docker-compose  -H ${__PDCI_DOCKER_HOST_TRN} --no-ansi  -p pdci_${__PDCI_APPLICATION_ENV} -f docker-compose.yml up -d
  echo ""
  echo ""
  docker-compose  -H ${__PDCI_DOCKER_HOST_TRN} --no-ansi  -p pdci_${__PDCI_APPLICATION_ENV} -f docker-compose.yml ps

}

echo "" >> .env
echo "PDCI_APP_VERSAO=${PDCI_GIT_BRANCH}.${BUILD_NUMBER}.(${GIT_COMMIT})" >> .env


docker  -H ${__PDCI_DOCKER_HOST_TRN} logout

echo "${__pdci_registry_gitlab_deploy_token_password}" | docker  -H ${__PDCI_DOCKER_HOST_TRN} login --username ${__pdci_registry_gitlab_deploy_token_username} --password-stdin ${PDCI_REGISTRY}

sed -i "s|[\$]{PDCI_APPLICATION_ENV}|${__PDCI_APPLICATION_ENV}|g" docker-compose.yml
sed -i "s|[\$]{__pdci_nome_projeto}|${__pdci_nome_projeto}|g" docker-compose.yml


deploy_service

rm -f export.env
echo ""
echo ""
cat .env
#rm -f .env
echo ""
echo ""
echo "docker  -H ${__PDCI_DOCKER_HOST_TRN} image prune -f"
docker  -H ${__PDCI_DOCKER_HOST_TRN} image prune -f
echo ""
echo ""
echo ""
rm -f lista_servicos
echo ""
echo ""


sleep 5
verificar_status_container () {
docker-compose  -H ${__PDCI_DOCKER_HOST_TRN} -p pdci_${__PDCI_APPLICATION_ENV} -f docker-compose.yml ps | awk -F " " ' { print $1 }' | sed "1d" | sed "1d" > lista_servicos
cat lista_servicos | while read line
do
	echo ""
    echo " Service => $line"
    echo ""

    container_id=$(docker -H ${__PDCI_DOCKER_HOST_TRN}  ps | grep $line | awk -F " " ' { print $1 }')
    container_image_id=""
    container_image=""
    container_error=""
    container_logs=""
    if [ -n ${container_id} ]; then

    	container_image=$(docker -H ${__PDCI_DOCKER_HOST_TRN}  ps -a | grep $line | awk -F " " ' { print $2 }')

        if [ "${container_id}" == "" ]; then
            echo ""
            echo "Lendo log do container"
        	container_log_id=$(docker -H ${__PDCI_DOCKER_HOST_TRN}  ps -a | grep $line | awk -F " " ' { print $1 }')
        	container_logs=$(docker -H ${__PDCI_DOCKER_HOST_TRN}  logs ${container_log_id})
            container_error=$(docker -H ${__PDCI_DOCKER_HOST_TRN}  container inspect  --format='{{.State.Error}}' ${container_log_id})
			__nome_file_inspect=${container_log_id}_error_container_inspect
            docker -H ${__PDCI_DOCKER_HOST_TRN}  container inspect  ${container_log_id} > ${__nome_file_inspect}
        else

          if [ "${container_image}" == "${__registry_projeto}" ]; then
            echo ""
            echo ""
            echo "DESABILITADO imagem de rollback"

           # echo "Rotular imagem de rollback"
           # docker  -H ${__PDCI_DOCKER_HOST_TRN} tag ${__registry_projeto} ${__registry_projeto}-rollback
            echo ""
            echo ""

          fi

        fi

        echo "*****************************"
        echo "Setando limit de cpf e memoria"
		echo ""

        case ${container_image} in
            *"app_sintax_ipt:"*)
                echo ""
                echo "--memory 5120m --memory-swap 5120m --cpus=\"8\""
                echo ""
                docker -H ${__PDCI_DOCKER_HOST_TRN} update --memory 5120m --memory-swap 5120m --cpus="4"  ${container_id}
                ;;
            *"app_monitora/enketo:"*)
                echo ""
                echo "--memory 2048m --memory-swap 2048m --cpus=\"2\""
                echo ""
                docker -H ${__PDCI_DOCKER_HOST_TRN} update --memory 2048m --memory-swap 2048m --cpus="2"  ${container_id}
                ;;
            *"app_monitora/monitora_ipt:"*)
                echo ""
                echo "--memory 5120m --memory-swap 5120m --cpus=\"4\""
                echo ""
                docker -H ${__PDCI_DOCKER_HOST_TRN} update --memory 2048m --memory-swap 2048m --cpus="4"  ${container_id}
                ;;

            *"app_lafsisbio:"*)
                echo ""
                echo "--memory 3072m --memory-swap 3072m --cpus=\"4\""
                echo ""
                docker -H ${__PDCI_DOCKER_HOST_TRN} update --memory 3072m --memory-swap 3072m --cpus="4"  ${container_id}
                ;;
            *"sentry"*)
                echo ""
                echo "--memory 3072m --memory-swap 3072m --cpus=\"4\""
                echo ""
                docker -H ${__PDCI_DOCKER_HOST_TRN} update --memory 3072m --memory-swap 3072m --cpus="4"  ${container_id}
                ;;
             *)
                echo ""
                echo "--memory 3072m --memory-swap 3072m --cpus=\"4\""
                echo ""
                docker -H ${__PDCI_DOCKER_HOST_TRN} update --memory 3072m --memory-swap 3072m --cpus="4"  ${container_id}
             #   deploy_service_configurar
                ;;
        esac

		echo ""
        echo "*****************************"
        echo ""
        echo ""



        echo "     container_id       => ${container_id}"
        echo "     container_image_id => ${container_image_id}"
        echo "     container_image    => ${container_image}"
        echo "     container_error    => ${container_error}"
        echo "     container_logs     => ${container_logs}"

    else
      echo "ERRO :: O conteiner do servico $line nao subiu. Verifique o problema"
      exit 1
    fi

    echo ""
done
}

verificar_status_container


rm -rf ./.env