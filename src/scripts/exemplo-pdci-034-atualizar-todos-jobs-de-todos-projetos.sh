#!/bin/bash

set -e

let count_app=1
descovery_rebuild_last_version(){
#  printf '\n\n  %s\n'  "================================================================================= "
#  printf '\n\n  %s\n'  "Verificando se existe a view ${__PDCI_JENKINS_APP_NAME} .. "
#  echo "curl -s 'http://127.0.0.1:8080/view/all/api/json' --header \"$(wget -q --auth-no-challenge --user ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN} --output-document - 'http://127.0.0.1:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)')\" --user ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} " > cmd_sh_view.sh
#echo "
#import jenkins.model.*
#import hudson.model.*
#Jenkins.instance.views.each {
#  view ->
#  println \"${view.name}\"
#}
#
#return" >  list_view.groovy
#JENKINS_URL=http://127.0.0.1:8080
#wget --no-check-certificate  ${JENKINS_URL}/jnlpJars/jenkins-cli.jar
#java -jar jenkins-cli.jar -s ${JENKINS_URL} groovy list_view.groovy --username ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN}
#exit 1

  printf '\n\n  %s\n'  "================================================================================= "
  printf '\n\n  %s\n'  "Discovery  Rebuild ${__PDCI_JENKINS_APP_NAME} .. "
  echo "Rebuild ${__PDCI_JENKINS_APP_NAME} .. " >>  output.txt
  ##BUSCA O ULTIMO NUMERO DO BUILD RODADO
  printf '    %s\n'  "BUSCA O ULTIMO NUMERO DO BUILD RODADO .."
  __PDCI_CMD_LAST_BUILD_NUMBER="curl -s 'http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/api/json' --header  $(wget -q --auth-no-challenge --user ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN} --output-document - 'http://127.0.0.1:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)') --user ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} | jq -r '.lastBuild.number'"
  echo "${__PDCI_CMD_LAST_BUILD_NUMBER}" >> cmd_last_number_build.sh
  #printf '\n      %s\n'  "__PDCI_CMD => ${__PDCI_CMD}"
  __PDCI_LAST_BUILD_NUMBER=$(eval ${__PDCI_CMD_LAST_BUILD_NUMBER} || true)
  printf '    %s\n'  " Ultimo numero da build => ${__PDCI_LAST_BUILD_NUMBER} ."

  if [ "${__PDCI_LAST_BUILD_NUMBER}" == "null" ] || [ "${__PDCI_LAST_BUILD_NUMBER}" == "" ] ; then

   echo "  Não foi encontrado nenhuma build de atualização rodada." >> output.txt
   echo "    Pulando atualização de sql para o app." >> output.txt
   printf '    %s\n'  "Não foi encontrado nenhuma build de atualização rodada."
   printf '    %s\n'  "    Pulando atualização de sql para o app. \n"

  else
    printf '    %s\n'  "Recupera a ultima versão rodada no referido build. .."
    ##Recupera a ultima versão rodada no referido build.
#    __PDCI_CMD="curl -s 'http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_LAST_BUILD_NUMBER}/api/json'  --header ${__PDCI_JENKINS_CRUMB} --user ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} | jq -r '.actions[].lastBuiltRevision | select(.branch)  | .branch[].name'"
    #__PDCI_CMD="curl -s 'http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_LAST_BUILD_NUMBER}/api/json'  --header ${__PDCI_JENKINS_CRUMB} --user ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} |  jq -r '.actions[] | select(.parameters)  | .parameters[].value'"
    __PDCI_CMD="curl -s 'http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_LAST_BUILD_NUMBER}/api/json'  --header  $(wget -q --auth-no-challenge --user ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN} --output-document - 'http://127.0.0.1:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)') --user ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} |  jq -r '.actions[].parameters[]? | select(.name==\"__PDCI_BRANCH\").value'"
    echo ${__PDCI_CMD} >> cmd_recupera_ultima_versao.sh
#    printf '\n    %s\n'  "__PDCI_CMD => ${__PDCI_CMD}"
    echo "      Recupera a ultima versão rodada no referido build. .." >> output.txt
    echo "        ${__PDCI_CMD}" >> output.txt
    __PDCI_REBUILD_VERSION=$(eval ${__PDCI_CMD})
    printf '    %s\n'  "Rebuild version => ${__PDCI_REBUILD_VERSION}"
    echo "Rebuild version => ${__PDCI_REBUILD_VERSION}" >> output.txt

    if [ "${__PDCI_REBUILD_VERSION}" != "" ] ; then
      printf '    %s\n'  "Iniciado job de atualizaçao de sql da aplicação ${__PDCI_JENKINS_APP_NAME} .."
      echo  "Iniciado job de atualizaçao de sql da aplicação ${__PDCI_JENKINS_APP_NAME} .." >> output.txt
      ##Iniciado job de atualizaçao de sql da aplicação.
#      __PDCI_CMD="curl -X POST http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/buildWithParameters  --user ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN}  --header ${__PDCI_JENKINS_CRUMB}  --data-urlencode json='{\"parameter\": [{\"name\":\"__PDCI_BRANCH_REBUILD\", \"value\":\"${__PDCI_REBUILD_VERSION}\"}]}'"

       #__PDCI_CMD="curl -X POST http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/build  --user ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN}  --header ${__PDCI_JENKINS_CRUMB}  --data-urlencode json='{\"parameter\": [{\"name\":\"__PDCI_BRANCH\", \"value\":\"${__PDCI_REBUILD_VERSION}\"}]}'"

#__PDCI_JENKINS_CRUMB=$(wget -q --auth-no-challenge --user ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN} --output-document - 'http://127.0.0.1:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)')
#echo "__PDCI_JENKINS_CRUMB => '${__PDCI_JENKINS_CRUMB}'"

       __PDCI_CMD="curl -X POST http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/build  --user ${__JENKINS_USER_ID}:11ddb9aa0d13e417b1296e52c6958677b4 --header $(wget -q --auth-no-challenge --user ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN} --output-document - 'http://127.0.0.1:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)')  --data-urlencode json='{\"parameter\": [{\"name\":\"__PDCI_BRANCH\", \"value\":\"${__PDCI_REBUILD_VERSION}\"},{\"name\":\"PDCI_DISPLAY_SQL\", \"value\":\"\"},{\"name\":\"PDCI_OBS\", \"value\":\"Aplicação atualizada após restauração do ambiente TCTI.\"}]}'"

      #printf '\n    %s\n'  "__PDCI_CMD => ${__PDCI_CMD}"
      echo  " ${__PDCI_CMD} " >> output.txt
      eval ${__PDCI_CMD}
      __PDCI_LAST_BUILD_NUMBER=$(eval ${__PDCI_CMD_LAST_BUILD_NUMBER})

# Espera enquanto o job disparado nao foi rodado.
#

      limit=320
        __PDCI_AFTER_LAST_BUILD_NUMBER=$(eval ${__PDCI_CMD_LAST_BUILD_NUMBER})
        count=0
        while [ "${__PDCI_LAST_BUILD_NUMBER}" ==  "${__PDCI_AFTER_LAST_BUILD_NUMBER}" ]; do

          if [ ${count} -lt ${limit} ]; then
#             CMD_VERIFICACAO_GUEUE=$(eval "curl -s 'http://127.0.0.1:8080/queue/api/json?pretty=true' --header Jenkins-Crumb:b238a088c0dcae1c2063484782bed31189b106ef9f9cab8b295a4543c5d58a3b --user  ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} | jq -r '.items[].task | select (.name=="${__PDCI_JENKINS_APP_NAME}").name'")

#          VERIFICA_QUEUE=$(eval "curl -s 'http://127.0.0.1:8080/queue/api/json?pretty=true' --header Jenkins-Crumb:b238a088c0dcae1c2063484782bed31189b106ef9f9cab8b295a4543c5d58a3b --user  ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} | jq -r '.items[].task | select (.name==\"pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db\").name'");
#          if [ "${VERIFICA_QUEUE}" == "pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db" ]; then
#            echo "Job no queue"
#          else
#            echo "Job nao esta mais no queue"
#          fi
#            eval "curl -s 'http://127.0.0.1:8080/queue/api/json?pretty=true' --header Jenkins-Crumb:b238a088c0dcae1c2063484782bed31189b106ef9f9cab8b295a4543c5d58a3b --user  ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} | jq -r '.items[].task | select (.name==\"pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db\").name'"
            echo "****************************************************"
            echo " Esperando a construção do job  em   ${count}/${limit} segundos"
            echo "****************************************************"
            echo ""
            let count=count+1
            sleep 1
            VERIFICA_QUEUE=$(eval "curl -s 'http://127.0.0.1:8080/queue/api/json?pretty=true' --header Jenkins-Crumb:b238a088c0dcae1c2063484782bed31189b106ef9f9cab8b295a4543c5d58a3b --user  ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} | jq -r '.items[].task | select (.name==\"pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db\").name'");
          if [ "${VERIFICA_QUEUE}" == "pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db" ]; then
            echo ""
            echo "   pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db :: Job na fila de espera. "
            echo ""
          else
            echo ""
            echo "   pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db :: Job saiu da fila de espera."
            echo ""

            let count=limit+0
#          fi
#            __PDCI_AFTER_LAST_BUILD_NUMBER=$(eval ${__PDCI_CMD_LAST_BUILD_NUMBER})
#            if [ "${__PDCI_LAST_BUILD_NUMBER}" ==  "${__PDCI_AFTER_LAST_BUILD_NUMBER}" ]; then
#             echo "..."
#            else


#             echo "curl -s 'http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER}/api/json'  --header  $(wget -q --auth-no-challenge --user ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN} --output-document - 'http://127.0.0.1:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)') --user ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} |  jq -r '.actions[] | select(.parameters)  | .parameters[].value'"
#             eval "curl -s 'http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER}/api/json'  --header  $(wget -q --auth-no-challenge --user ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN} --output-document - 'http://127.0.0.1:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)') --user ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} |  jq -r '.actions[] | select(.parameters)  | .parameters[].value'"
             count_verificacao=0
             limit_veridficacao=30
             __PDCI_AFTER_LAST_BUILD_NUMBER=$(eval ${__PDCI_CMD_LAST_BUILD_NUMBER})
             echo ""
             echo "job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER} :: Iniciado a construção do job .  "
             echo ""
             CMD_VERIFICACAO=$(eval "curl -s 'http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER}/api/json?pretty=true' --header Jenkins-Crumb:b238a088c0dcae1c2063484782bed31189b106ef9f9cab8b295a4543c5d58a3b --user  ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} | jq -r '.building'")
              while [ "${CMD_VERIFICACAO}" == "true" ]; do
                echo ""
                echo "job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER} :: (${CMD_VERIFICACAO}) Esperando finalização da execução do job."
                echo ""
                sleep 1
                CMD_VERIFICACAO=$(eval "curl -s 'http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER}/api/json?pretty=true' --header Jenkins-Crumb:b238a088c0dcae1c2063484782bed31189b106ef9f9cab8b295a4543c5d58a3b --user  ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} | jq -r '.building' || false")
             done
             echo ""
             echo "job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER} ::  Job Finalizado."
             echo ""
             RESULT_BUILD=$(eval "curl -s 'http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER}/api/json?pretty=true' --header Jenkins-Crumb:b238a088c0dcae1c2063484782bed31189b106ef9f9cab8b295a4543c5d58a3b --user  ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} | jq -r '.result'")
             echo ""
             if [ "${RESULT_BUILD}" == "SUCCESS" ]; then
               echo ""
               echo "job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER} ::  Job executado com sucesso => /job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER}/ "
               echo "job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER} ::  Job executado com sucesso => /job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER}/ "
               echo "${count_app}) ${__PDCI_JENKINS_APP_NAME}
                          Resultado da construção:${RESULT_BUILD}
                          Console do resultado: https://jenkins.icmbio.gov.br/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER}/console

               ">>resumo-${RESULT_BUILD}.txt
             else
               echo ""
               echo "job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER} :: Job com problema (RESULT_BUILD => ${RESULT_BUILD}). Verifique em https://jenkins.icmbio.gov.br/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER}/console "
               echo ""
               echo "${count_app}) ${__PDCI_JENKINS_APP_NAME}
                          Resultado da construção:${RESULT_BUILD}
                          Console do resultado: https://jenkins.icmbio.gov.br/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER}/console

               ">>resumo-${RESULT_BUILD}.txt
             fi
             echo ""
             echo "---------------------------------------------------------------------------------------------------"
             echo ""
#             while [ ${count_verificacao} -lt ${limit_veridficacao} ]; do
#                let count_verificacao=count_verificacao+1
#                CMD_VERIFICACAO="curl -s 'http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER}/api/json?pretty=true' --header Jenkins-Crumb:b238a088c0dcae1c2063484782bed31189b106ef9f9cab8b295a4543c5d58a3b --user  ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} | jq -r '.building'"
#                echo "*********************************"
#                eval ${CMD_VERIFICACAO}
#               echo "*********************************"
#             done
#
##             curl -s 'http://127.0.0.1:8080/view/${__PDCI_JENKINS_APP_NAME}/job/pdci-${__PDCI_JENKINS_APP_NAME}-020-upd-sql-em-TCTI-db/${__PDCI_AFTER_LAST_BUILD_NUMBER}/api/json'  --header  $(wget -q --auth-no-challenge --user ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN} --output-document - 'http://127.0.0.1:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)') --user ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} |  jq -r '.actions[] | select(.parameters)  | .parameters[].value'
#
            fi
          else
            __PDCI_AFTER_LAST_BUILD_NUMBER="ESGOTOU O TEMPO DE ESPERA"
            echo "Tempo esgotado de esperar do job sair da fila de espera e entrar em execução."
            echo ""
               echo "${count_app}) ${__PDCI_JENKINS_APP_NAME}
                          Resultado: Tempo esgotado de esperar do job sair da fila de espera e entrar em execução.

               ">>resumo-${RESULT_BUILD}.txt
          fi

        done

    else
      printf '    %s\n'  "Pulou build com parametro por nao ter nenhuma build contruida ate este momento."
      echo  "Pulou build com parâmetro por não ter nenhuma build contruida até este momento." >> output.txt
      echo ""
               echo "${count_app}) ${__PDCI_JENKINS_APP_NAME}
                          Resultado: Pulou build com parâmetro por não ter nenhuma build contruida até este momento.

               ">>resumo-${RESULT_BUILD}.txt
    fi

  fi
(( count_app++ )) || true
}

#__PDCI_JENKINS_CRUMB=$(wget -q --auth-no-challenge --user ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN} --output-document - 'http://127.0.0.1:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)')
#echo "__PDCI_JENKINS_CRUMB => '$(wget -q --auth-no-challenge --user ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN} --output-document - 'http://127.0.0.1:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)')'"

echo " "
echo " Recuperando lista de projetos conforme informada nas opções existente em PDCI_URL_GITLAB_PROJETO no job exemplo-pdci-033-criar-jobs-para-projeto."
echo " "

#DEMAIS APP
echo "curl -s 'http://127.0.0.1:8080/view/all/api/json' --header $(wget -q --auth-no-challenge --user ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN} --output-document - 'http://127.0.0.1:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)') --user ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} | jq -r '.jobs[] | select( ( .name  | contains(\"-020-upd-sql-em-TCTI\") ) ) | .name' > list_jobs.txt" > cmd-get-list-jobs
curl -s 'http://127.0.0.1:8080/view/all/api/json' --header "$(wget -q --auth-no-challenge --user ${__JENKINS_USER_ID} --password ${__JENKINS_API_TOKEN} --output-document - 'http://127.0.0.1:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)')" --user ${__JENKINS_USER_ID}:${__JENKINS_API_TOKEN} | jq -r '.jobs[] | select( ( .name  | contains("-020-upd-sql-em-TCTI") ) ) | .name' > list_jobs.txt

while IFS="" read -r p || [ -n "$p" ]
do
  __PDCI_JENKINS_APP_NAME=$(echo $p | sed -e "s/pdci-//g" | sed -e "s/-020-upd-sql-em-TCTI-db//g")

  if [ "exemplo-{app_nome}" != "${__PDCI_JENKINS_APP_NAME}" ] ; then
     printf '%s\n' "__PDCI_JENKINS_APP_NAME => ${__PDCI_JENKINS_APP_NAME}"

    arrayOriginal=$(echo "${arr_all_app_requirements[@]}")
    arrayResult=$(echo "${arr_all_app_requirements[@]//${__PDCI_JENKINS_APP_NAME}/}")
#    echo "arr_all_app_requirements => ${arr_all_app_requirements[@]}"
#    echo "arrayOriginal => ${arrayOriginal}"
#    echo "arrayResult=> ${arrayResult}"
    if [ "${arrayOriginal}" == "${arrayResult}" ] ; then
       printf '\n\n  %s\n'  "Nao possui no requirements de app ${__PDCI_JENKINS_APP_NAME}.. "
       descovery_rebuild_last_version
    else
      printf '\n\n  %s\n'  "Possui requirements de app .. ${__PDCI_JENKINS_APP_NAME}"
    fi


  else
    printf ' Ignorado script  de exemplo. =>  %s\n'  "__PDCI_JENKINS_APP_NAME => ${__PDCI_JENKINS_APP_NAME}"
  fi
#  exit 1
done <  list_jobs.txt
#termo="resumo-*";
#cmd_param_ls="${termo}"
for i in $( ls resumo-* ); do
  if [[ -f "$i" ]]; then
    echo ""
    echo "Lendo arquivo de resumo ${i}"
    echo ""
    cat  ${i}
    echo ""
  fi
done
#cat resumo-${RESULT_BUILD}.txt
#rm -rf resultado*.txt
rm -rf output.txt