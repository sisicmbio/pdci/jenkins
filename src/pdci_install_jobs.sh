#!/bin/sh
echo ""
echo ""
echo "Download de exemplo a ser usado: $i"
echo ""
git clone https://gitlab+deploy-token-50060:d7RApxRvoVjaYrRcYzkR@gitlab.com/pdci/pdci-jobs-jenkins.git
cd pdci-jobs-jenkins
__git_rev_parse=$(git rev-parse  HEAD)
__git_show_date=$(git show -s --format=%ci master^{commit})
for i in `ls exemplo*.xml`
do
	echo "Job de exemplo a ser usado: $i"

    nome_job_novo=$(echo $i | sed -e "s/.xml//g" )

    mkdir -p /usr/share/jenkins/ref/jobs/${nome_job_novo}/
#    sed -i "s|__GIT_GIT_JOB_COMMIT_SHA|${__git_rev_parse}|g"  $i
#    sed -i "s|__GIT_JOB_COMMIT_TIME|${__git_show_date}|g"   $i
    cp $i /usr/share/jenkins/ref/jobs/${nome_job_novo}/config.xml
done
echo ""
echo ""
#echo "Processando credencials"
#for i in `ls credentials/*.xml`
#do
#    echo ""
#	echo "   Credentials de exemplo a ser usado: $i"
#
#    nome_credential=$(echo $i | sed -e "s|credentials/||g" )
#
#    cp $i /usr/share/jenkins/ref/${nome_credential}
#
#done
echo ""
echo ""
echo "Processando views"
for i in `ls views/*.xml`
do
    echo ""
        echo "   Credentials de exemplo a ser usado: $i"

    nome_views=$(echo $i | sed -e "s|views/||g" )

    cp $i /usr/share/jenkins/ref/${nome_views}

done

cd ..

rm -rf pdci-jobs-jenkins

chown  jenkins:jenkins /usr/share/jenkins/ref/