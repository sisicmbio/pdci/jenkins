#!/usr/bin/env bash

set -ex
ls -lah /var/dump/
df -h 
#rm -rf /var/dump/db_sisicmbio_PRO_full_*
#exit 1
copiar_files_dump () {

	echo ""
    echo " Descobrir container jenkins em tcp://10.197.32.76:2375 ."
    echo ""
    echo ""
    echo ""

    container_id=$(docker -H  tcp://10.197.32.76:2375  ps | grep pdci_suite_production_jenkins_dood | awk -F " " ' { print $1 }')
   

    if [ -n ${container_id} ]; then

     
     
        echo ""
        echo ""
		echo ""
        echo "*****************************"
        echo "Iniciar processo de compactacao do /var/jenkins_home/ "
		echo ""
docker -H tcp://10.197.32.76:2375 exec -u root   ${container_id} bash -c ' /pdci_clean_workspace_jobs.sh '

docker -H tcp://10.197.32.76:2375 exec -u root   ${container_id} bash -c 'pwd ; cd /var/dump/;  tar -czf jenkins_home.tar.gz  /var/jenkins_home ; ls -lah;'
        
        echo ""
        echo ""
        echo "*****************************"
        echo "Iniciar Copia do arquivo compactado do files dump ."
		echo ""     

docker -H tcp://10.197.32.76:2375 cp  ${container_id}:/var/dump/jenkins_home.tar.gz  /jenkins_home.tar.gz

        echo ""
        echo ""
		    echo ""
        echo "*****************************"
        echo " Removendo arquivo compactado do dump em tcp://10.197.32.76:2375"
        echo ""  


    else
      echo "ERRO :: Nao foi possivel descobrir o container jenkins em tcp://10.197.32.76:2375 ."
      exit 1
    fi

}

copiar_files_dump


